<style type="text/css">
	select[multiple] {
		height: 200px;
	}
	.ms-container {
		width: 100%;
	}
</style>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">
        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Products</li>
                </ul>
                <!--breadcrumbs end -->
        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>

        				<h4 class="panel-title pull-right">
			                <a class="btn btn-info mtop20" data-toggle="modal" href="#addProduct">Add</a>
			            </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Name</th>
        								<th>tag_line</th>
        								<th>Category</th>
        								<th>Application</th>
        								<th>Price</th>
        								<th>Image</th>
        								<th>Actions</th>
        							</tr>
        						</thead>
        						<tbody>
	        						<?php
	        						$i = 1;
	        						foreach ($products as $product) {
	        						?>
        							<tr>
        								<td><?php echo $i++; ?></td>
        								<td><?php echo $product->name; ?></td>
        								<td><?php echo $product->tag_line; ?></td>
        								<td><?php echo implode(" / ", $product->sub_cat); ?></td>
        								<td><?php echo implode(" / ", $product->sub_app); ?></td>
        								<td><?php echo $product->price; ?></td>
        								<td>
        									<img src="<?php echo $product->image; ?>" style="width: 100px; height: 100px;">
        								</td>
        								<td>
	        								<a href="<?php echo base_url('/admin/product/') . $product->product_id; ?>">Edit</a>
	        								<a href="javascript:void(0);" onclick="deleteProduct(<?php echo $product->product_id; ?>)">Delete</a>
        								</td>
        							</tr>
        							<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- add product modal start -->
        <div class="modal fade " id="addProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/addProduct'); ?>" enctype="multipart/form-data">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Product</h4>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="form-group">
		                		<label>Name</label>
		                		<input type="text" class="form-control" placeholder="Name" name="name" required>
		                	</div>
		                	<div class="form-group">
		                		<label>Description</label>
		                		<textarea class="wysihtml5 form-control" name="description"></textarea>
		                	</div>
		                	<div class="form-group">
		                		<label>Tag Line</label>
		                		<textarea class="wysihtml5 form-control" name="tag_line"></textarea>
		                	</div>
		                	<div class="form-group">
		                		<label>Price</label>
		                		<input type="number" name="price" class="form-control" required step="0.01">
		                	</div>
		                	<div class="form-group">
		                		<label>Package Size</label>
		                		<input type="text" name="package_size" class="form-control" required>
		                	</div>
		                	<div class="form-group">
		                		<label>Storage Life</label>
		                		<input type="text" name="storage_life" class="form-control" required>
		                	</div>
		                	<div class="form-group">
		                		<label class="control-label">Brand</label>
		                		<div>
		                			<select class="form-control m-bot15" name="brand_id">
		                				<?php
		                				foreach ($brands as $brand) {
		                				?>
		                				<option value="<?php echo $brand->brand_id; ?>"><?php echo $brand->name; ?></option>
		                				<?php
		                				}
		                				?>
		                			</select>
		                		</div>
		                	</div>
		                	<div class="form-group">
                                <label class="control-label">Category</label>
                                <div>
                                    <select class="multi-select" id="my_multi_select1" name="sub_cat_id[]" multiple required>
                                        <?php
		                				foreach ($categories as $category) {
		                				?>
		                				<optgroup label="<?php echo $category->name; ?>">
		                					<?php
		                					foreach ($sub_categories as $sub_cat) {
		                						if ($category->category_id == $sub_cat->category_id) {
		                					?>
			                					<option value="<?php echo $sub_cat->sub_cat_id; ?>"><?php echo $sub_cat->name; ?></option>
			                				<?php
			                					}
			                				}
			                				?>
		                				</optgroup>
		                				<?php
		                				}
		                				?>
                                    </select>
                                </div>
                            </div>
		                	<!-- <div class="form-group">
		                		<label class="control-label">Category</label>
		                		<div>
		                			<select class="form-control m-bot15" name="sub_cat_id[]" multiple required>
		                				<?php
		                				foreach ($categories as $category) {
		                				?>
		                				<optgroup label="<?php echo $category->name; ?>">
		                					<?php
		                					foreach ($sub_categories as $sub_cat) {
		                						if ($category->category_id == $sub_cat->category_id) {
		                					?>
			                					<option value="<?php echo $sub_cat->sub_cat_id; ?>"><?php echo $sub_cat->name; ?></option>
			                				<?php
			                					}
			                				}
			                				?>
		                				</optgroup>
		                				<?php
		                				}
		                				?>
		                			</select>
		                		</div>
		                	</div> -->
		                	<div class="form-group">
                                <label class="control-label">Application</label>
                                <div>
                                    <select class="multi-select" id="my_multi_select2" name="sub_app_id[]" multiple required>
                                        <?php
		                				foreach ($applications as $application) {
		                				?>
		                				<optgroup label="<?php echo $application->name; ?>">
		                					<?php
		                					foreach ($sub_applications as $sub_app) {
		                						if ($application->application_id == $sub_app->application_id) {
		                					?>
		                						<option value="<?php echo $sub_app->sub_app_id; ?>"><?php echo $sub_app->name; ?></option>
		                					<?php
		                						}
		                					}
		                					?>
		                				</optgroup>
		                				<?php
		                				}
		                				?>
                                    </select>
                                </div>
                            </div>
		                	<!-- <div class="form-group">
		                		<label class="control-label">Application</label>
		                		<div>
		                			<select class="form-control m-bot15" name="sub_app_id[]" multiple required>
		                				<?php
		                				foreach ($applications as $application) {
		                				?>
		                				<optgroup label="<?php echo $application->name; ?>">
		                					<?php
		                					foreach ($sub_applications as $sub_app) {
		                						if ($application->application_id == $sub_app->application_id) {
		                					?>
		                						<option value="<?php echo $sub_app->sub_app_id; ?>"><?php echo $sub_app->name; ?></option>
		                					<?php
		                						}
		                					}
		                					?>
		                				</optgroup>
		                				<?php
		                				}
		                				?>
		                			</select>
		                		</div>
		                	</div> -->
		                	<div class="form-group">
                                <label class="control-label">Image Upload</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="image" class="default" required/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
		                		<label>Featured</label>
		                		<div class="form">
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="1"> Yes
			                		</label>
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="0" checked> No
			                		</label>
		                		</div>
		                	</div>
		                	<div class="form-group">
		                		<label></label>
		                		<div class="form">
			                		<label class="checkbox-inline">
			                			<input type="checkbox" name="notif"> Notify Users?
			                		</label>
		                		</div>
		                	</div>
	                    </div>
	                    <div class="modal-footer">
	                        <!-- <button data-dismiss="modal" class="btn btn-default" type="button">Close</button> -->
	                        <button type="Submit" class="btn btn-info btn-block">Submit</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add product modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
	function deleteProduct(id) {
		if (confirm("Are you sure you want to delete this?")) {
			window.location = "<?php echo base_url(); ?>"+"/admin/deleteProduct/"+id;
		}
	}
</script>