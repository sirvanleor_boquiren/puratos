<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Branches</li>
                </ul>
                <!--breadcrumbs end -->

        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>
                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addBranch">Add</a>
                        </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
                                        <th>Branch Name</th>
        								<th>Website</th>
        								<th>Email</th>
                                        <th>Phone</th>
                                        <th>Fax</th>
                                        <th>Viber</th>
        								<th>Address</th>
                                        <th>Country</th>
                                        <th>Main Branch</th>
        								<th>Actions</th>
        							</tr>
        						</thead>
        						<tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($branches as $branch) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $branch->name; ?></td>
                                        <td><?php echo $branch->website; ?></td>
                                        <td><?php echo $branch->email; ?></td>
                                        <td><?php echo $branch->phone; ?></td>
                                        <td><?php echo $branch->fax; ?></td>
                                        <td><?php echo $branch->viber; ?></td>
                                        <td><?php echo $branch->street . " " . $branch->city . " " . $branch->state; ?></td>
                                        <td>
                                            <?php
                                            foreach ($countries as $country) {
                                                if ($country->country_id == $branch->country_id) {
                                                    echo $country->name;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td> <?php echo ($branch->is_main_branch == 1) ? "Yes" : "No"; ?> </td>
                                        <td>
                                            <a href="<?php echo base_url('/admin/branch/') . $branch->branch_id; ?>">Edit</a>
                                            <a href="javascript:void(0);" onclick="deleteBranch(<?php echo $branch->branch_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- add product modal start -->
        <div class="modal fade " id="addBranch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addBranch'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Branch</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Branch Name</label>
                                <input type="text" class="form-control" placeholder="Branch Name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email Address" name="email">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="url" class="form-control" placeholder="Website" name="website">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" placeholder="Phone" name="phone">
                            </div>
                            <div class="form-group">
                                <label>Fax</label>
                                <input type="text" class="form-control" placeholder="Fax" name="fax">
                            </div>
                            <div class="form-group">
                                <label>Viber</label>
                                <input type="text" class="form-control" placeholder="Viber" name="viber">
                            </div>
                            <div class="form-group">
                                <label>Street</label>
                                <input type="text" class="form-control" placeholder="Street" name="street">
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="City" name="city">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" placeholder="State" name="state">
                            </div>
                            <div class="form-group">
                                <label>Zipcode</label>
                                <input type="text" class="form-control" placeholder="Zipcode" name="zipcode">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <div>
                                    <select class="form-control m-bot15" name="country_id">
                                    <option>SELECT</option>
                                        <?php
                                        foreach ($countries as $country) {
                                        ?>
                                        <option value="<?php echo $country->country_id; ?>"><?php echo $country->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Main Branch</label>
                                <div>
                                    <select class="form-control m-bot15" name="is_main_branch">
                                        <option value="1">Yes</option>
                                        <option value="0" selected>No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add product modal end -->

        <!-- page end-->
    </section>
</section>
<!-- main content end -->

<script type="text/javascript">
    function deleteBranch(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteBranch/"+id;
        }
    }
</script>