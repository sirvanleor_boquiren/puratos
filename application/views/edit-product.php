<style type="text/css">
    select[multiple] {
        height: 200px;
    }
    .ms-container {
        width: 100%;
    }
</style>
<?php
// var_dump($product);
// die();
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/products') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Product</li>
                </ul>
                <!--breadcrumbs end -->
         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateProduct/') . $product->product_id; ?>" enctype="multipart/form-data">
         					<div class="form-group">
         						<label>Name</label>
         						<input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $product->name; ?>">
         					</div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea class="wysihtml5 form-control" name="description"><?php echo $product->description; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Tag Line</label>
                                <textarea class="wysihtml5 form-control" name="tag_line"><?php echo $product->tag_line; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Price</label>
                                <input type="number" name="price" class="form-control" value="<?php echo $product->price; ?>" step="0.01">
                            </div>
                            <div class="form-group">
                                <label>Package Size</label>
                                <input type="text" name="package_size" class="form-control" value="<?php echo $product->package_size; ?>">
                            </div>
                            <div class="form-group">
                                <label>Storage Life</label>
                                <input type="text" name="storage_life" class="form-control" value="<?php echo $product->storage_life; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Brand</label>
                                <div>
                                    <select class="form-control m-bot15" name="brand_id">
                                        <?php
                                        foreach ($brands as $brand) {
                                        ?>
                                        <option value="<?php echo $brand->brand_id; ?>" <?php echo ($brand->brand_id == $product->brand_id) ? "selected" : ""; ?>><?php echo $brand->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <div>
                                    <select class="multi-select" id="my_multi_select1" name="sub_cat_id[]" multiple required>
                                        <?php
                                        foreach ($categories as $category) {
                                        ?>
                                        <optgroup label="<?php echo $category->name; ?>">
                                            <?php
                                            foreach ($sub_categories as $sub_cat) {
                                                if ($category->category_id == $sub_cat->category_id) {
                                            ?>
                                                <option value="<?php echo $sub_cat->sub_cat_id; ?>" <?php echo (in_array($sub_cat->name, $product->sub_cat)) ? "selected" : ""; ?>><?php echo $sub_cat->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </optgroup>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label">Category</label>
                                <div>
                                    <select class="form-control m-bot15" name="sub_cat_id[]" multiple required>
                                        <?php
                                        foreach ($categories as $category) {
                                        ?>
                                        <optgroup label="<?php echo $category->name; ?>">
                                            <?php
                                            foreach ($sub_categories as $sub_cat) {
                                                if ($category->category_id == $sub_cat->category_id) {
                                            ?>
                                                <option value="<?php echo $sub_cat->sub_cat_id; ?>" <?php echo (in_array($sub_cat->name, $product->sub_cat)) ? "selected" : ""; ?>><?php echo $sub_cat->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </optgroup>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label">Application</label>
                                <div>
                                    <select class="multi-select" id="my_multi_select2" name="sub_app_id[]" multiple required>
                                        <?php
                                        foreach ($applications as $application) {
                                        ?>
                                        <optgroup label="<?php echo $application->name; ?>">
                                            <?php
                                            foreach ($sub_applications as $sub_app) {
                                                if ($application->application_id == $sub_app->application_id) {
                                            ?>
                                                <option value="<?php echo $sub_app->sub_app_id; ?>" <?php echo (in_array($sub_app->name, $product->sub_app)) ? "selected" : ""; ?> ><?php echo $sub_app->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </optgroup>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="control-label">Application</label>
                                <div>
                                    <select class="form-control m-bot15" name="sub_app_id[]" multiple required>
                                        <?php
                                        foreach ($applications as $application) {
                                        ?>
                                        <optgroup label="<?php echo $application->name; ?>">
                                            <?php
                                            foreach ($sub_applications as $sub_app) {
                                                if ($application->application_id == $sub_app->application_id) {
                                            ?>
                                                <option value="<?php echo $sub_app->sub_app_id; ?>" <?php echo (in_array($sub_app->name, $product->sub_app)) ? "selected" : ""; ?> ><?php echo $sub_app->name; ?></option>
                                            <?php
                                                }
                                            }
                                            ?>
                                        </optgroup>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="control-label">Image Upload</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?php echo ($product->image != "") ? $product->image : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" ?>" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="image" class="default" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Featured</label>
                                <div class="form">
                                    <label class="checkbox-inline">
                                        <input type="radio" name="is_featured" value="1" <?php echo ($product->is_featured == 1) ? "checked" : ""; ?>> Yes
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="is_featured" value="0" <?php echo ($product->is_featured == 0) ? "checked" : ""; ?>> No
                                    </label>
                                </div>
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->