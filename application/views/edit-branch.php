<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/branches') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Branch</li>
                </ul>
                <!--breadcrumbs end -->
                <section class="panel">
                    <header class="panel-heading">
                        <?php //echo uri_string(); ?>
                        <?php
                        // var_dump($schedule);
                        $days = array();
                        foreach ($schedule as $sched) {
                            $days[] = $sched->day;
                        }
                        ?>
                    </header>
                    <div class="panel-body">
                        <form role="form" method="POST" action="<?php echo base_url('admin/addBranchSchedule/') . $branch->branch_id; ?>">
                            <div class="form-group col-lg-6">
                                <label class="control-label">Start Time</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" class="form-control timepicker-default" name="start_time" required="" placeholder="HH:MM PD">
                                      <span class="input-group-btn">
                                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                      </span>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <label class="control-label">End Time</label>
                                <div class="input-group bootstrap-timepicker">
                                    <input type="text" class="form-control timepicker-default" name="end_time" required="" placeholder="HH:MM PD">
                                      <span class="input-group-btn">
                                      <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
                                      </span>
                                </div>
                            </div>
                            <div class="form-group col-lg-12">
                                <label class="control-label">Office Hours</label>
                                <div>
                                    <select class="form-control m-bot15" name="days[]" multiple>
                                        <?php if (!in_array(0, $days)) { ?>
                                        <option value="0">Sunday</option>
                                        <?php } ?>

                                        <?php if (!in_array(1, $days)) { ?>
                                        <option value="1">Monday</option>
                                        <?php } ?>

                                        <?php if (!in_array(2, $days)) { ?>
                                        <option value="2">Tuesday</option>
                                        <?php } ?>

                                        <?php if (!in_array(3, $days)) { ?>
                                        <option value="3">Wednesday</option>
                                        <?php } ?>

                                        <?php if (!in_array(4, $days)) { ?>
                                        <option value="4">Thursday</option>
                                        <?php } ?>

                                        <?php if (!in_array(5, $days)) { ?>
                                        <option value="5">Friday</option>
                                        <?php } ?>

                                        <?php if (!in_array(6, $days)) { ?>
                                        <option value="6">Saturday</option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-lg-6">
                                <button type="Submit" class="btn btn-success">Save</button>
                            </div>
                        </form>
                        <div class="form-group col-lg-12">
                            <div class="adv-table">
                                <table class="display table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Day</th>
                                            <th>Start Time</th>
                                            <th>End Time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($schedule as $sched) {
                                        ?>
                                        <tr>
                                            <td><?php echo $sched->day; ?></td>
                                            <td><?php echo $sched->start_time; ?></td>
                                            <td><?php echo $sched->end_time; ?></td>
                                            <td>
                                                <form style='display:inline;' onsubmit="return confirm('Are you sure you want to delete that?');" action="<?php echo base_url('admin/deleteBranchSchedule/') . $sched->id; ?>" method="post" class="no-loader">
                                                    <input type="hidden" name="branch_id" value="<?php echo $branch->branch_id; ?>">
                                                    <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-o "></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

         		<section class="panel">
         			<header class="panel-heading">
         				<?php echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateBranch/') . $branch->branch_id; ?>">
                            <div class="form-group">
                                <label>Branch Name</label>
                                <input type="text" class="form-control" placeholder="Branch Name" name="name" value="<?php echo $branch->name; ?>">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email Address" name="email" value="<?php echo $branch->email; ?>">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="url" class="form-control" placeholder="Website" name="website" value="<?php echo $branch->website; ?>">
                            </div>
                            <div class="form-group">
                                <label>Phone</label>
                                <input type="text" class="form-control" placeholder="Phone" name="phone" value="<?php echo $branch->phone; ?>">
                            </div>
                            <div class="form-group">
                                <label>Fax</label>
                                <input type="text" class="form-control" placeholder="Fax" name="fax" value="<?php echo $branch->fax; ?>">
                            </div>
                            <div class="form-group">
                                <label>Viber</label>
                                <input type="text" class="form-control" placeholder="Viber" name="viber" value="<?php echo $branch->viber; ?>">
                            </div>
                            <div class="form-group">
                                <label>Street</label>
                                <input type="text" class="form-control" placeholder="Street" name="street" value="<?php echo $branch->street; ?>">
                            </div>
                            <div class="form-group">
                                <label>City</label>
                                <input type="text" class="form-control" placeholder="City" name="city" value="<?php echo $branch->city; ?>">
                            </div>
                            <div class="form-group">
                                <label>State</label>
                                <input type="text" class="form-control" placeholder="State" name="state" value="<?php echo $branch->state; ?>">
                            </div>
                            <div class="form-group">
                                <label>Zipcode</label>
                                <input type="text" class="form-control" placeholder="Zipcode" name="zipcode" value="<?php echo $branch->zipcode; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Country</label>
                                <div>
                                    <select class="form-control m-bot15" name="country_id">
                                        <option>SELECT</option>
                                        <?php
                                        foreach ($countries as $country) {
                                        ?>
                                        <option value="<?php echo $country->country_id; ?>" <?php echo ($branch->country_id == $country->country_id) ? "selected" : ""; ?>><?php echo $country->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Main Branch</label>
                                <div>
                                    <select class="form-control m-bot15" name="is_main_branch">
                                        <option value="1" <?php echo ($branch->is_main_branch == 1) ? 'selected' : ''; ?>>Yes</option>
                                        <option value="0" <?php echo ($branch->is_main_branch == 0) ? 'selected' : ''; ?>>No</option>
                                    </select>
                                </div>
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

