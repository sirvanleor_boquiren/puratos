<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- table start -->
        <div class="row">
        	<div class="col-sm-12">
        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Segments</li>
                </ul>
                <!--breadcrumbs end -->
        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>

						<h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addSegment">Add</a>
                        </h4>
                        
                        <!-- add segment modal start -->
				        <div class="modal fade " id="addSegment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				            <div class="modal-dialog">
				                <div class="modal-content">
				                    <form method="POST" action="<?php echo base_url('admin/addSegment'); ?>">
				                        <div class="modal-header">
				                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				                            <h4 class="modal-title">Add Segment</h4>
				                        </div>
				                        <div class="modal-body">
				                            <div class="form-group">
				                                <label>Name</label>
				                                <input type="text" class="form-control" placeholder="Segment" name="name" required>
				                            </div>
				                        </div>
				                        <div class="modal-footer">
				                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
				                        </div>
				                    </form>
				                </div>
				            </div>
				        </div>
				        <!-- add segment modal end -->

        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Name</th>
        								<th>Action</th>
        							</tr>
        						</thead>
        						<tbody>
        							<?php
        							$i = 1;
        							foreach ($segments as $segment) {
        							?>
        							<tr>
        								<td><?php echo $i++; ?></td>
        								<td><?php echo $segment->name; ?></td>
        								<td>
        									<a href="#editSegment" class="edit-segment" data-toggle="modal"
        									data-id="<?php echo $segment->segment_id; ?>"
        									data-name="<?php echo $segment->name; ?>">Edit</a> /
                                            <a href="javascript:void(0);" onclick="deleteSegment(<?php echo $segment->segment_id; ?>)">Delete</a>
        								</td>
        							</tr>
        							<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        

        <!-- edit segment modal start -->
        <div class="modal fade " id="editSegment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/updateSegment'); ?>">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Edit Segment</h4>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="form-group">
	                    		<label>Name</label>
	                    		<input type="text" class="form-control" id="name" name="name">
	                    	</div>
	                    </div>
	                    <div class="modal-footer">
	                    	<input type="hidden" id="segment_id" name="segment_id">
	                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                        <button class="btn btn-success" type="submit">Save changes</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit segment modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
	$(function(){
		$(".edit-segment").on("click", function(){
			$(".modal-footer #segment_id").val($(this).data("id"));
			$(".modal-body #name").val($(this).data("name"));
		});
	});

    function deleteSegment(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteSegment/"+id;
        }
    }
</script>