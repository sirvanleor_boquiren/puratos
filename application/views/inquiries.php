<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Inquiries</li>
                </ul>
                <!--breadcrumbs end -->

        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Message</th>
        							</tr>
        						</thead>
        						<tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($inquiries as $inquiry) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $inquiry->name; ?></td>
                                        <td><?php echo $inquiry->email; ?></td>
                                        <td><?php echo $inquiry->mobile; ?></td>
                                        <td><?php echo $inquiry->message; ?></td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->