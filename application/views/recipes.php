<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Recipes</li>
                </ul>
                <!--breadcrumbs end -->

        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>

        				<h4 class="panel-title pull-right">
			                <a class="btn btn-info mtop20" data-toggle="modal" href="#addRecipes">Add</a>
			            </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Title</th>
        								<th>Tag Line</th>
        								<th>Image</th>
        								<th>Instructions</th>
        								<th>Category</th>
        								<th>Servings</th>
        								<th>Actions</th>
        							</tr>
        						</thead>
        						<tbody>
        							<?php
        							$i=1;
        							foreach ($recipes as $recipe) {
        							?>
        							<tr>
	        							<td><?php echo $i++; ?></td>
	        							<td><?php echo $recipe->title; ?></td>
	        							<td><?php echo $recipe->tag_line; ?></td>
	        							<td>
        									<img src="<?php echo $recipe->image; ?>" style="width: 100px; height: 100px;">
        								</td>
	        							<td><?php echo $recipe->instructions; ?></td>
	        							<td>
	        								<?php
	        								foreach ($categories as $category) {
	        									if ($category->category_id == $recipe->category_id) {
	        										echo $category->name;
	        									}
	        								}
	        								?>
	        							</td>
	        							<td><?php echo $recipe->servings; ?></td>
	        							<td>
	        								<a href="<?php echo base_url('/admin/recipe/') . $recipe->recipe_id; ?>">Edit</a>
	        								<a href="javascript:void(0);" onclick="deleteRecipe(<?php echo $recipe->recipe_id; ?>)">Delete</a>
	        							</td>
	        						</tr>
	        						<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- add recipes modal start -->
        <div class="modal fade " id="addRecipes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/addRecipe'); ?>" enctype="multipart/form-data">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Recipes</h4>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="form-group">
		                		<label>Title</label>
		                		<input type="text" class="form-control" placeholder="Title" name="title" required>
		                	</div>

		                	<div class="form-group">
		                		<label>Tag Line</label>
		                		<input type="text" class="form-control" placeholder="Tag line" name="tag_line">
		                	</div>
		                	
		                	<div class="form-group">
                                <label class="control-label">Image Upload</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="image" class="default" required/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
		                	<!-- <div class="form-group">
		                		<label>Image</label><br>
		                		<div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                      	<span class="btn btn-white btn-file">
                                      		<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                      		<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                      		<input type="file" class="default" name="image" required/>
                                      	</span>
                                      	<span class="fileupload-preview" style="margin-left:5px;"></span>
                                    	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div><br><br>
		                	</div> -->

		                	<div class="form-group">
                                <label class="control-label">Puratos Ingredients</label><br>
                                <label class="control-label">
                                    <a href="javascript:void(0);" id="add_product">Add</a>
                                </label>
                                <div class="inputs_product_outer">
                                	<div class="inputs_product m-bot15">
	                                    <div class="col-sm-8 form-group">
	                                    	<select class="form-control" name="puratos_ingredients[]">
	                                    		<option>SELECT</option>
	                                    		<?php
	                                    		foreach ($products as $product) {
	                                    		?>
	                                    		<option value="<?php echo $product->product_id; ?>"><?php echo $product->name; ?></option>
	                                    		<?php
	                                    		}
	                                    		?>
	                                    	</select>
	                                    </div>
	                                    <div class="col-sm-3 form-group"><input type="text" class="form-control" name="puratos_ingre_amount[]" value="" placeholder="100g."></div>
	                                </div>
                                </div>
                            </div>

		                	<div class="form-group">
                                <label class="control-label">Other Ingredients</label><br>
                                <label class="control-label">
                                    <a href="javascript:void(0);" id="add_other">Add</a>
                                </label>
                                <div class="inputs_other_outer">
	                                <div class="inputs_other">
	                                    <div class="col-sm-8 form-group"><input type="text" class="form-control" name="other_ingredients[]" value="" placeholder="i.e. Sugar"></div>
	                                    <div class="col-sm-3 form-group"><input type="text" class="form-control" name="other_amount[]" value="" placeholder="100g."></div>
	                                </div>
                                </div>
                            </div>

		                	<div class="form-group">
		                		<label>Instructions</label>
		                		<textarea class="wysihtml5 form-control" name="instructions"></textarea>
		                	</div>

		                	<div class="form-group">
		                		<label class="control-label">Category</label>
		                		<div>
		                			<select class="form-control m-bot15" name="category_id">
		                				<option>SELECT</option>
		                				<?php
		                				foreach ($categories as $category) {
		                				?>
		                				<option value="<?php echo $category->category_id; ?>"><?php echo $category->name; ?></option>
		                				<?php
		                				}
		                				?>
		                			</select>
		                		</div>
		                	</div>

		                	<div class="form-group">
		                		<label class="control-label">Servings</label>
		                		<input type="text" class="form-control" placeholder="Servings" name="servings">
		                	</div>

		                	<div class="form-group">
		                		<label>Featured</label>
		                		<div class="form">
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="1"> Yes
			                		</label>
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="0" checked> No
			                		</label>
		                		</div>
		                	</div>

	                    </div>
	                    <div class="modal-footer">
	                        <button type="Submit" class="btn btn-info btn-block">Submit</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add news modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
function deleteRecipe(id) {
	if (confirm("Are you sure you want to delete this?")) {
		window.location = "<?php echo base_url(); ?>"+"/admin/deleteRecipe/"+id;
	}
}

var j = $('input').size() + 1;
$('#add_product').click(function() {
    $(`<div class="inputs_product m-bot15" id="product_`+j+`">
    	<div class="col-sm-8 form-group">
    	`+ `
    	<?php
    	echo '<select class="form-control" name="puratos_ingredients[]">';
    	echo '<option>SELECT</option>';
    	foreach ($products as $product) {
    		echo '<option value="' . $product->product_id . '">' . $product->name . '</option>';
    	}
    	echo '</select>';
    	?>`
    	+`
    	</div>
    	<div class="col-sm-3 form-group">
    		<input type="text" class="form-control" name="puratos_ingre_amount[]" value="" placeholder="100g.">
    	</div>
    	<div class="col-sm-1"><a href="javascript:void(0);" onclick="deleteIngredients('product_`+j+`')"><i class="fa fa-times"></i></a></div>
       </div>`).fadeIn('slow').appendTo('.inputs_product_outer');
});

function deleteIngredients(id)
{
    $('#'+id).fadeOut(400, function(){
        $('#'+id).remove();
    });
}
</script>