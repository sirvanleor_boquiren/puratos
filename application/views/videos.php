<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">
        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Videos</li>
                </ul>
                <!--breadcrumbs end -->
        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>

        				<h4 class="panel-title pull-right">
			                <a class="btn btn-info mtop20" data-toggle="modal" href="#addVideo">Add</a>
			            </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Title</th>
        								<th>Thumbnail</th>
        								<th>Video Link</th>
        								<th>Description</th>
        								<th>Tag Line</th>
        								<th>Featured</th>
        								<th>Actions</th>
        							</tr>
        						</thead>
        						<tbody>
        							<?php
        							$i=1;
        							foreach ($videos as $vid) {
        							?>
        							<tr>
	        							<td><?php echo $i++; ?></td>
	        							<td><?php echo $vid->title; ?></td>
	        							<!-- <td><?php echo $vid->thumbnail; ?></td> -->
	        							<td>
        									<img src="<?php echo $vid->thumbnail; ?>" style="width: 100px; height: 100px;">
        								</td>
	        							<td><?php echo $vid->vid_link; ?></td>
	        							<td><?php echo $vid->description; ?></td>
	        							<td><?php echo $vid->tag_line; ?></td>
	        							<td><?php echo $vid->is_featured; ?></td>
	        							<td>
	        								<a href="<?php echo base_url('/admin/video/') . $vid->video_id; ?>">Edit</a>
	        								<a href="javascript:void(0);" onclick="deleteVideo(<?php echo $vid->video_id; ?>)">Delete</a>
	        							</td>
	        						</tr>
	        						<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- add video modal start -->
        <div class="modal fade " id="addVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/addVideo'); ?>" enctype="multipart/form-data">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Add Video</h4>
	                    </div>
	                    <div class="modal-body">
		                	<div class="form-group">
		                		<label>Title</label>
		                		<input type="text" class="form-control" placeholder="Title" name="title" required>
		                	</div>
		                	<div class="form-group">
                                <label class="control-label">Image Upload</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="thumbnail" class="default" required/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
		                	<!-- <div class="form-group">
		                		<label>Thumbnail</label><br>
		                		<div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                      	<span class="btn btn-white btn-file">
                                      		<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                      		<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                      		<input type="file" class="default" name="thumbnail" required/>
                                      	</span>
                                      	<span class="fileupload-preview" style="margin-left:5px;"></span>
                                    	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div><br><br>
		                	</div> -->
		                	<div class="form-group">
		                		<label>Video Link</label>
		                		<input type="text" class="form-control" placeholder="Video Link" name="vid_link" required>
		                	</div>
		                	<div class="form-group">
		                		<label>Description</label>
		                		<textarea class="wysihtml5 form-control" name="description"></textarea>
		                	</div>
		                	<div class="form-group">
		                		<label>Tag Line</label>
		                		<input type="text" class="form-control" placeholder="Tag Line" name="tag_line">
		                	</div>
		                	<div class="form-group">
		                		<label>Featured</label>
		                		<div class="form">
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="1"> Yes
			                		</label>
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="0" checked> No
			                		</label>
		                		</div>
		                	</div>
	                    </div>
	                    <div class="modal-footer">
	                        <!-- <button data-dismiss="modal" class="btn btn-default" type="button">Close</button> -->
	                        <button type="Submit" class="btn btn-info btn-block">Submit</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add video modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
	function deleteVideo(id) {
		if (confirm("Are you sure you want to delete this?")) {
			window.location = "<?php echo base_url(); ?>"+"/admin/deleteVideo/"+id;
		}
	}
</script>