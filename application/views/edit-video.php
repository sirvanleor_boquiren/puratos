main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
         		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/videos') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Video</li>
                </ul>
                <!--breadcrumbs end -->
         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateVideo/') . $video_id; ?>" enctype="multipart/form-data">
         					<div class="form-group">
         						<label>Title</label>
         						<input type="text" class="form-control" placeholder="Title" name="title" value="<?= $title ?>">
         					</div>
         					<div class="form-group">
                                <label class="control-label">Image Upload</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?php echo ($thumbnail != "") ? $thumbnail : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" ?>" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="thumbnail" class="default" required/>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
         					<!-- <div class="form-group">
         						<label>Thumbnail</label><br>
         						<div class="controls col-md-9">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                      	<span class="btn btn-white btn-file">
                                      		<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select file</span>
                                      		<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                      		<input type="file" class="default" name="thumbnail"/>
                                      	</span>
                                      	<span class="fileupload-preview" style="margin-left:5px;"></span>
                                    	<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
                                    </div>
                                </div><br><br>
         					</div> -->
         					<div class="form-group">
		                		<label>Video Link</label>
		                		<input type="text" class="form-control" placeholder="Video Link" name="vid_link" value="<?= $vid_link ?>">
		                	</div>
		                	<div class="form-group">
		                		<label>Description</label>
		                		<textarea class="wysihtml5 form-control" name="description"><?= $description ?></textarea>
		                	</div>
		                	<div class="form-group">
		                		<label>Tag Line</label>
		                		<input type="text" class="form-control" placeholder="Tag Line" name="tag_line" value="<?= $tag_line ?>">
		                	</div>
		                	<div class="form-group">
		                		<label>Featured</label>
		                		<div class="form">
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="1" <?php echo ($is_featured == 1) ? "checked" : ""; ?>> Yes
			                		</label>
			                		<label class="checkbox-inline">
			                			<input type="radio" name="is_featured" value="0" <?php echo ($is_featured == 0) ? "checked" : ""; ?>> No
			                		</label>
		                		</div>
		                	</div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end