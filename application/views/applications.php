<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Applications</li>
                </ul>
                <!--breadcrumbs end -->

                <!-- Main Application Table -->
                <section class="panel">
                	<header class="panel-heading">
                		<?php echo "Main - Applications"; ?>

                		<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addApplication">Add</a>
                        </h4>
                	</header>
                	<div class="panel-body">
                		<div class="adv-table">
                			<table class="display table table-bordered table-striped" id="dynamic-table">
                				<thead>
                					<tr>
                						<th>#</th>
                						<th>Name</th>
                						<th>Action</th>
                					</tr>
                				</thead>
                				<tbody>
                					<?php
                					$i = 1;
                					foreach ($applications as $application) {
                					?>
                					<tr>
                						<td><?php echo $i++; ?></td>
                						<td><?php echo $application->name ?></td>
                						<td>
                                            <a href="#editApplication" class="edit-application" data-toggle="modal"
                                            data-id="<?php echo $application->application_id; ?>"
                                            data-name="<?php echo $application->name; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteApplication(<?php echo $application->application_id; ?>)">Delete</a>
                                        </td>
                					</tr>
                					<?php
                					}
                					?>
                				</tbody>
                			</table>
                		</div>
                	</div>
                </section>
                <!-- Main Application Table END -->

                <!-- Sub Application Table -->
                <section class="panel">
        			<header class="panel-heading">
        				<?php echo "Sub - Applications"; ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addSubApplication">Add</a>
                        </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table2">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Name</th>
        								<th>Main application</th>
        								<th>Action</th>
        							</tr>
        						</thead>
        						<tbody>
        							<?php
        							$i = 1;
        							foreach ($sub_applications as $sub_application) {
        							?>
        							<tr>
        								<td><?php echo $i++; ?></td>
        								<td><?php echo $sub_application->name; ?></td>
        								<td>
        									<?php
        									foreach ($applications as $application) {
        										if ($sub_application->application_id == $application->application_id) {
        											echo $application->name;
        										}
        									}
        									?>
        								</td>
        								<td>
        									<a href="<?php echo base_url('/admin/application/') . $sub_application->sub_app_id; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteSubApplication(<?php echo $sub_application->sub_app_id; ?>)">Delete</a>
        								</td>
        							</tr>
        							<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
                <!-- Sub Application Table END -->

        	</div>
        </div>
        <!-- table end -->

        <!-- add main application modal start -->
        <div class="modal fade " id="addApplication" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/addApplication') ?>">
                		<div class="modal-header">
                			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Main Application</h4>
                		</div>
                		<div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Application Name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                	</form>
                </div>
            </div>
        </div>
        <!-- add main application modal end -->

        <!-- edit main application modal start -->
        <div class="modal fade " id="editApplication" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/updateApplication'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Application</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="application_id" name="application_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit main application modal end -->

        <!-- add sub-application modal start -->
        <div class="modal fade " id="addSubApplication" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/addSubApplication'); ?>">
                		<div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Sub-Application</h4>
                        </div>
                        <div class="modal-body">
                        	<div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Sub-Application Name" name="name">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Applications</label>
                                <div>
                                    <select class="form-control m-bot15" name="application_id">
                                        <?php
                                        foreach ($applications as $application) {
                                        ?>
                                        <option value="<?php echo $application->application_id; ?>"><?php echo $application->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                	</form>
                </div>
            </div>
        </div>
        <!-- add sub-application modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
    $(function(){
        $(".edit-application").on("click", function(){
            $(".modal-footer #application_id").val($(this).data("id"));
            $(".modal-body #name").val($(this).data("name"));
        });
    });

    function deleteApplication(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteApplication/"+id;
        }
    }

    function deleteSubApplication(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteSubApplication/"+id;
        }
    }
</script>