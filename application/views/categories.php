<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         
        <!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Categories</li>
                </ul>
                <!--breadcrumbs end -->

                <!-- Main Category Table -->
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo "Main - categories" ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addCategory">Add</a>
                        </h4>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($categories as $category) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $category->name; ?></td>
                                        <td>
                                            <a href="#editCategory" class="edit-category" data-toggle="modal"
                                            data-id="<?php echo $category->category_id; ?>"
                                            data-name="<?php echo $category->name; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteCategory(<?php echo $category->category_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
                <!-- Main Category Table END -->

                <!-- Sub Category Table -->
        		<section class="panel">
        			<header class="panel-heading">
        				<?php echo "Sub - categories" ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addSubCategory">Add</a>
                        </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Main Category</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($sub_categories as $sub_category) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $sub_category->name; ?></td>
                                        <td>
                                            <?php
                                            foreach ($categories as $category) {
                                                if ($sub_category->category_id == $category->category_id) {
                                                echo $category->name;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo base_url('/admin/category/') . $sub_category->sub_cat_id; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteSubCategory(<?php echo $sub_category->sub_cat_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
        				</div>
        			</div>
        		</section>
                <!-- Sub Category Table END -->

        	</div>
        </div>
        <!-- table end -->

        <!-- add main category modal start -->
        <div class="modal fade " id="addCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addCategory'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Main Category</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Category Name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add main category modal end -->

        <!-- edit main category modal start -->
        <div class="modal fade " id="editCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/updateCategory'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Category</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="category_id" name="category_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit main category modal end -->

        <!-- add sub-category modal start -->
        <div class="modal fade " id="addSubCategory" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addSubCategory'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Sub-Category</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Sub-Category Name" name="name">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Categories</label>
                                <div>
                                    <select class="form-control m-bot15" name="category_id">
                                        <?php
                                        foreach ($categories as $category) {
                                        ?>
                                        <option value="<?php echo $category->category_id; ?>"><?php echo $category->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add sub-category modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
    $(function(){
        $(".edit-category").on("click", function(){
            $(".modal-footer #category_id").val($(this).data("id"));
            $(".modal-body #name").val($(this).data("name"));
        });
    });

    function deleteCategory(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteCategory/"+id;
        }
    }

    function deleteSubCategory(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteSubCategory/"+id;
        }
    }
</script>