<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">
        		<!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Users</li>
                </ul>
                <!--breadcrumbs end -->
        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>
						
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
        								<th>Name</th>
        								<th>Username</th>
        								<th>Email</th>
        								<th>Mobile</th>
        								<th>Actions</th>
        							</tr>
        						</thead>
        						<tbody>
        							<?php
        							$i = 1;
        							foreach ($users as $user) {
        							?>
        							<tr>
        								<td><?php echo $i++; ?></td>
        								<td><?php echo $user->lname . ", " . $user->fname; ?></td>
        								<td><?php echo $user->username; ?></td>
        								<td><?php echo $user->email; ?></td>
        								<td><?php echo $user->mobile; ?></td>
        								<td>
        									<a href="#editUser" class="edit-user" data-toggle="modal" 
        									data-id="<?php echo $user->user_id; ?>"
        									data-title="<?php echo $user->title; ?>"
        									data-lname="<?php echo $user->lname; ?>"
        									data-fname="<?php echo $user->fname; ?>"
        									data-email="<?php echo $user->email; ?>"
        									data-username="<?php echo $user->username; ?>"
        									data-mobile="<?php echo $user->mobile; ?>"
        									data-address="<?php echo $user->address; ?>"
        									data-company="<?php echo $user->company; ?>"
        									data-birthday="<?php echo $user->birthday; ?>"
        									data-segment="<?php echo $user->segment; ?>"
        									>Edit</a> / 
        									<a href="javascript:void(0);" onclick="deleteUser(<?php echo $user->user_id; ?>)">Delete</a>
        								</td>
        							</tr>
        							<?php
        							}
        							?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- edit user modal start -->
        <div class="modal fade " id="editUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/updateUser'); ?>">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Edit User</h4>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="form-group">
	                    		<label>Title</label>
	                    		<input type="text" class="form-control" id="title" name="title">
	                    	</div>
	                    	<div class="form-group">
	                    		<label>First Name</label>
	                    		<input type="text" class="form-control" id="fname" name="fname" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Last Name</label>
	                    		<input type="text" class="form-control" id="lname" name="lname" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Email Address</label>
	                    		<input type="text" class="form-control" id="email" name="email" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Username</label>
	                    		<input type="text" class="form-control" id="username" name="username" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Mobile</label>
	                    		<input type="text" class="form-control" id="mobile" name="mobile" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Address</label>
	                    		<input type="text" class="form-control" id="address" name="address" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Company</label>
	                    		<input type="text" class="form-control" id="company" name="company" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Birthday</label>
	                    		<input type="date" class="form-control" id="birthday" name="birthday" required>
	                    	</div>
	                    	<div class="form-group">
	                    		<label>Segment</label>
	                    		<input type="text" class="form-control" id="segment" name="segment" required>
	                    	</div>
	                    </div>
	                    <div class="modal-footer">
	                    	<input type="hidden" id="user_id" name="user_id">
	                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                        <button class="btn btn-success" type="submit">Save changes</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit user modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end -->

<script type="text/javascript">
	$(function(){
		$(".edit-user").on("click", function(){
			$(".modal-footer #user_id").val($(this).data("id"));
			$(".modal-body #title").val($(this).data("title"));
			$(".modal-body #fname").val($(this).data("fname"));
			$(".modal-body #lname").val($(this).data("lname"));
			$(".modal-body #email").val($(this).data("email"));
			$(".modal-body #username").val($(this).data("username"));
			$(".modal-body #mobile").val($(this).data("mobile"));
			$(".modal-body #address").val($(this).data("address"));
			$(".modal-body #company").val($(this).data("company"));
			$(".modal-body #birthday").val($(this).data("birthday"));
			$(".modal-body #segment").val($(this).data("segment"));
		});
	});

	function deleteUser(id) {
        if (confirm("Are you sure you want to delete this user?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteUser/"+id;
        }
    }
</script>