<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/applications') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Sub Application</li>
                </ul>
                <!--breadcrumbs end -->
         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateSubApplication/') . $sub_application->sub_app_id; ?>">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Sub Application Name" name="name" value="<?php echo $sub_application->name; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Application</label>
                                <div>
                                    <select class="form-control m-bot15" name="application_id">
                                        <?php
                                        foreach ($applications as $application) {
                                        ?>
                                        <option value="<?php echo $application->application_id; ?>" <?php echo ($application->application_id == $sub_application->application_id) ? "selected" : ""; ?>><?php echo $application->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

