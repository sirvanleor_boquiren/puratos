<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

        <!-- table start -->
        <div class="row">
            <div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Dealers</li>
                </ul>
                <!--breadcrumbs end -->

                <section class="panel">
                    <header class="panel-heading">
                        <?php //echo uri_string(); ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addDealer">Add</a>
                        </h4>
                    </header>
                    <div class="panel-body">
                        <div class="adv-table">
                            <table class="display table table-bordered table-striped" id="dynamic-table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Address</th>
                                        <th>Contact</th>
                                        <th>Email</th>
                                        <th>Website</th>
                                        <th>Coordiates</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($dealers as $dealer) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $dealer->name; ?></td>
                                        <td><?php echo $dealer->address; ?></td>
                                        <td><?php echo $dealer->contact; ?></td>
                                        <td><?php echo $dealer->email; ?></td>
                                        <td><?php echo $dealer->website; ?></td>
                                        <td><?php echo $dealer->latitude . ", " . $dealer->longitude; ?></td>
                                        <td>
                                            <a href="#editDealer" class="edit-dealer" data-toggle="modal"
                                            data-id="<?php echo $dealer->dealer_id; ?>"
                                            data-name="<?php echo $dealer->name; ?>"
                                            data-address="<?php echo $dealer->address; ?>"
                                            data-contact="<?php echo $dealer->contact; ?>"
                                            data-email="<?php echo $dealer->email; ?>"
                                            data-website="<?php echo $dealer->website; ?>"
                                            data-longitude="<?php echo $dealer->longitude; ?>"
                                            data-latitude="<?php echo $dealer->latitude; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteDealer(<?php echo $dealer->dealer_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <!-- table end -->

        <!-- add dealer modal start -->
        <div class="modal fade " id="addDealer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addDealer'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Dealer</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" name="name" required>
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" placeholder="Address" name="address">
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <input type="text" class="form-control" placeholder="Contact" name="contact">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" placeholder="Email" name="email">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="url" class="form-control" placeholder="Website" name="website">
                            </div>
                            <div class="form-group">
                                <label>Latitude</label>
                                <input type="text" class="form-control" placeholder="" name="latitude">
                            </div>
                            <div class="form-group">
                                <label>Longitude</label>
                                <input type="text" class="form-control" placeholder="" name="longitude">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add dealer modal end -->

        <!-- edit dealer modal start -->
        <div class="modal fade " id="editDealer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/updateDealer'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Dealer</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label>Address</label>
                                <input type="text" class="form-control" id="address" name="address">
                            </div>
                            <div class="form-group">
                                <label>Contact</label>
                                <input type="text" class="form-control" id="contact" name="contact">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                            <div class="form-group">
                                <label>Website</label>
                                <input type="url" class="form-control" id="website" name="website">
                            </div>
                            <div class="form-group">
                                <label>Latitude</label>
                                <input type="text" class="form-control" id="latitude" name="latitude">
                            </div>
                            <div class="form-group">
                                <label>Longitude</label>
                                <input type="text" class="form-control" id="longitude" name="longitude">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="dealer_id" name="dealer_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit dealer modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
    $(function(){
        $(".edit-dealer").on("click", function(){
            $(".modal-footer #dealer_id").val($(this).data("id"));
            $(".modal-body #name").val($(this).data("name"));
            $(".modal-body #address").val($(this).data("address"));
            $(".modal-body #contact").val($(this).data("contact"));
            $(".modal-body #email").val($(this).data("email"));
            $(".modal-body #website").val($(this).data("website"));
            $(".modal-body #longitude").val($(this).data("longitude"));
            $(".modal-body #latitude").val($(this).data("latitude"));
        });
    });

    function deleteDealer(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteDealer/"+id;
        }
    }
</script>