<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->

		<!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Countries</li>
                </ul>
                <!--breadcrumbs end -->

        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addCountry">Add</a>
                        </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
                                        <th>Country</th>
                                        <th>Action</th>
        							</tr>
        						</thead>
        						<tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($countries as $country) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $country->name; ?></td>
                                        <td>
                                            <a href="#editCountry" class="edit-country" data-toggle="modal"
                                            data-id="<?php echo $country->country_id; ?>"
                                            data-name="<?php echo $country->name; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteCountry(<?php echo $country->country_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- edit country modal start -->
        <div class="modal fade " id="editCountry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/updateCountry'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Edit Country</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <input type="hidden" id="country_id" name="country_id">
                            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                            <button class="btn btn-success" type="submit">Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit country modal end -->

        <!-- add country modal start -->
        <div class="modal fade " id="addCountry" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addCountry'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Country</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Country" name="name" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add country modal end -->

        <!-- page end-->
    </section>
</section>
<!-- main content end -->

<script type="text/javascript">
    $(function(){
        $(".edit-country").on("click", function(){
            $(".modal-footer #country_id").val($(this).data("id"));
            $(".modal-body #name").val($(this).data("name"));
        });
    });

    function deleteCountry(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteCountry/"+id;
        }
    }
</script>