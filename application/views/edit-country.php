<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/countries') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Country</li>
                </ul>
                <!--breadcrumbs end -->
         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateCountry/') . $country->country_id; ?>">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Country" name="name" value="<?php echo $country->name; ?>">
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

