<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">
                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/categories') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Sub Category</li>
                </ul>
                <!--breadcrumbs end -->
         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateSubCategory/') . $sub_category->sub_cat_id; ?>">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Sub Category Name" name="name" value="<?php echo $sub_category->name; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label">Categories</label>
                                <div>
                                    <select class="form-control m-bot15" name="category_id">
                                        <?php
                                        foreach ($categories as $category) {
                                        ?>
                                        <option value="<?php echo $category->category_id; ?>" <?php echo ($category->category_id == $sub_category->category_id) ? "selected" : ""; ?>><?php echo $category->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->

