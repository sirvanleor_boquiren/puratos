<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         
        <!-- table start -->
        <div class="row">
        	<div class="col-sm-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Brands</li>
                </ul>
                <!--breadcrumbs end -->

        		<section class="panel">
        			<header class="panel-heading">
        				<?php //echo uri_string(); ?>

        				<span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
        					<?php echo $this->session->flashdata('alert_msg'); ?>
						</span>

                        <h4 class="panel-title pull-right">
                            <a class="btn btn-info mtop20" data-toggle="modal" href="#addBrand">Add</a>
                        </h4>
        			</header>
        			<div class="panel-body">
        				<div class="adv-table">
        					<table class="display table table-bordered table-striped" id="dynamic-table">
        						<thead>
        							<tr>
        								<th>#</th>
                                        <th>Brands</th>
                                        <th>Action</th>
        							</tr>
        						</thead>
        						<tbody>
                                    <?php
                                    $i = 1;
                                    foreach ($brands as $brand) {
                                    ?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $brand->name; ?></td>
                                        <td>
                                            <a href="#editBrand" class="edit-brand" data-toggle="modal"
                                            data-id="<?php echo $brand->brand_id; ?>"
                                            data-name="<?php echo $brand->name; ?>">Edit</a> / 
                                            <a href="javascript:void(0);" onclick="deleteBrand(<?php echo $brand->brand_id; ?>)">Delete</a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
        						</tbody>
        					</table>
        				</div>
        			</div>
        		</section>
        	</div>
        </div>
        <!-- table end -->

        <!-- edit brand modal start -->
        <div class="modal fade " id="editBrand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                	<form method="POST" action="<?php echo base_url('admin/updateBrand'); ?>">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title">Edit Brand</h4>
	                    </div>
	                    <div class="modal-body">
	                    	<div class="form-group">
	                    		<label>Name</label>
	                    		<input type="text" class="form-control" id="name" name="name" required>
	                    	</div>
	                    </div>
	                    <div class="modal-footer">
	                    	<input type="hidden" id="brand_id" name="brand_id">
	                        <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                        <button class="btn btn-success" type="submit">Save changes</button>
	                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- edit brand modal end -->

        <!-- add brand modal start -->
        <div class="modal fade " id="addBrand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form method="POST" action="<?php echo base_url('admin/addBrand'); ?>">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title">Add Brand</h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Brand Name" name="name" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="Submit" class="btn btn-info btn-block">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- add brand modal end -->

        <!-- page end-->
    </section>
</section>
<!--main content end-->

<script type="text/javascript">
	$(function(){
		$(".edit-brand").on("click", function(){
			$(".modal-footer #brand_id").val($(this).data("id"));
			$(".modal-body #name").val($(this).data("name"));
		});
	});

    function deleteBrand(id) {
        if (confirm("Are you sure you want to delete this?")) {
            window.location = "<?php echo base_url(); ?>"+"/admin/deleteBrand/"+id;
        }
    }
</script>