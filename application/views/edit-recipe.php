<!-- main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
         <div class="row">
         	<div class="col-lg-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li><a href="<?php echo base_url('admin/recipes') ?>"> &laquo; Back</a></li>
                    <li class="active">Edit Recipe</li>
                </ul>
                <!--breadcrumbs end -->

         		<section class="panel">
         			<header class="panel-heading">
         				<?php //echo uri_string(); ?>
         			</header>
         			<div class="panel-body">
         				<form role="form" method="POST" action="<?php echo base_url('admin/updateRecipe/') . $recipes->recipe_id; ?>" enctype="multipart/form-data">

         					<div class="form-group">
         						<label>Title</label>
         						<input type="text" class="form-control" placeholder="Title" name="title" value="<?php echo $recipes->title; ?>">
         					</div>

                            <div class="form-group">
                                <label>Tag Line</label>
                                <input type="text" class="form-control" name="tag_line" value="<?php echo $recipes->tag_line; ?>">
                            </div>

                            <div class="form-group">
                                <label class="control-label">Image</label>
                                <div>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                            <img src="<?php echo ($recipes->image != "") ? $recipes->image : "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" ?>" alt="" />
                                        </div>
                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                                <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                                <input type="file" name="image" class="default" />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Puratos Ingredients</label><br>
                                <label class="control-label">
                                    <a href="javascript:void(0);" id="add_product">Add</a>
                                </label>
                                <div class="inputs_product_outer">
                                    <?php
                                    $j = 1;
                                    foreach ($ingredients as $ingredient) {
                                    ?>
                                        <div class="inputs_product m-bot15" id="product_<?php echo $j; ?>">
                                            <div class="col-sm-8 form-group">
                                                <select class="form-control" name="puratos_ingredients[]">
                                                    <option>SELECT</option>
                                                    <?php
                                                    foreach ($products as $product) {
                                                    ?>
                                                    <option value="<?php echo $product->product_id; ?>" <?php echo ($product->product_id == $ingredient->product_id) ? "selected" : ""; ?>><?php echo $product->name; ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="col-sm-3 form-group"><input type="text" class="form-control" name="puratos_ingre_amount[]" value="<?php echo $ingredient->amount; ?>" placeholder="100g."></div>
                                            <div class="col-sm-1 form-group delete-ingre"><a href="javascript:void(0);" onclick="deleteIngredients('product_<?php echo $j++; ?>')"><i class="fa fa-times"></i></a></div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Other Ingredients</label><br>
                                <label class="control-label">
                                    <a href="javascript:void(0);" id="add_other">Add</a>
                                </label>

                                <div class="inputs_other_outer">
                                <?php
                                $i = 1;
                                foreach ($other_ingredients as $ingredients) {
                                ?>
                                <div class="inputs_other" id="other_<?php echo $i; ?>">
                                    <div class="col-lg-8 form-group"><input type="text" class="form-control" name="other_ingredients[]" value="<?php echo $ingredients->name; ?>" placeholder="i.e. Sugar"></div>
                                    <div class="col-lg-3 form-group"><input type="text" class="form-control" name="other_amount[]" value="<?php echo $ingredients->amount; ?>" placeholder="100g."></div>
                                    <div class="col-sm-1 form-group delete-ingre"><a href="javascript:void(0);" class="close-other-o" onclick="deleteIngredients('other_<?php echo $i++; ?>')"><i class="fa fa-times"></i></a></div>
                                </div>
                                <?php
                                }
                                ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Instructions</label>
                                <textarea class="wysihtml5 form-control" name="instructions" rows="10"><?php echo $recipes->instructions; ?></textarea>
                            </div>

                            <div class="form-group">
                                <label class="control-label">Category</label>
                                <div>
                                    <select class="form-control m-bot15" name="category_id">
                                        <?php
                                        foreach ($categories as $category) {
                                        ?>
                                        <option value="<?php echo $category->category_id; ?>" <?php echo ($category->category_id == $recipes->category_id) ? "selected" : ""; ?>><?php echo $category->name; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Servings</label>
                                <input type="text" class="form-control" name="servings" value="<?php echo $recipes->servings; ?>">
                            </div>
                            <div class="form-group">
                                <label>Featured</label>
                                <div class="form">
                                    <label class="checkbox-inline">
                                        <input type="radio" name="is_featured" value="1" <?php echo ($recipes->is_featured == 1) ? "checked" : ""; ?>> Yes
                                    </label>
                                    <label class="checkbox-inline">
                                        <input type="radio" name="is_featured" value="0" <?php echo ($recipes->is_featured == 0) ? "checked" : ""; ?>> No
                                    </label>
                                </div>
                            </div>
		                	<button type="Submit" class="btn btn-success btn-block">Save</button>
         				</form>
         			</div>
         		</section>
         	</div>
         </div>
        <!-- page end-->
    </section>
</section>
<!--main content end -->

<script type="text/javascript">
var j = $('input').size() + 1;
$('#add_product').click(function() {
    $(`<div class="inputs_product m-bot15" id="product_`+j+`">
        <div class="col-sm-8 form-group">
        `+ `
        <?php
        echo '<select class="form-control" name="puratos_ingredients[]">';
        echo '<option>SELECT</option>';
        foreach ($products as $product) {
            echo '<option value="' . $product->product_id . '">' . $product->name . '</option>';
        }
        echo '</select>';
        ?>`
        +`
        </div>
        <div class="col-sm-3 form-group">
            <input type="text" class="form-control" name="puratos_ingre_amount[]" value="" placeholder="100g.">
        </div>
        <div class="col-sm-1 form-group delete-ingre"><a href="javascript:void(0);" onclick="deleteIngredients('product_`+j+`')"><i class="fa fa-times"></i></a></div>
       </div>`).fadeIn('slow').appendTo('.inputs_product_outer');
});

function deleteIngredients(id)
{
    $('#'+id).fadeOut(400, function(){
        $('#'+id).remove();
    });
}
</script>