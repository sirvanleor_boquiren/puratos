<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-lg-12">

                <!--breadcrumbs start -->
                <ul class="breadcrumb">
                    <li class="active">Notification</li>
                </ul>
                <!--breadcrumbs end -->

                <section class="panel">
                    <header class="panel-heading">
                        <?php //echo uri_string(); ?>

                        <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                            <?php echo $this->session->flashdata('alert_msg'); ?>
                        </span>

                    </header>
                    <div class="panel-body">
                        <form method="POST" action="<?php echo base_url('admin/sendNotification'); ?>">
                            <div class="form-group col-sm-7">
                                <label>Title</label>
                                <input type="text" class="form-control" name="title">
                            </div>
                            <div class="form-group col-sm-7">
                                <label>Message</label>
                                <textarea class="form-control" name="message" rows="5" maxlength="500"></textarea>
                            </div>
                            <div class="form-group col-sm-7">
                                <label class="control-label">Type</label>
                                <div>
                                    <select class="form-control m-bot15" name="category">
                                        <option value="announcement">Announcement</option>
                                        <option value="promo">Promo/Sale</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-sm-7">
                                <input type="submit" class="btn btn-info" name="">
                            </div>

                        </form>
                    </div>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->