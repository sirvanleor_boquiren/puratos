<?php

class Product_model extends CI_model {

	public function __construct()
    {
    	parent::__construct();
    	$this->load->model('filter_model');
    }

	public function getProducts($page = NULL)
	{
		
		if ($page != NULL) {
			$max_products = 10;
			if ($page == 1) {
				$limit = "LIMIT $max_products";
				$page = 0;
				$this->db->limit($max_products, $page);
			}
			else {
				$page = ($page - 1) * $max_products;
				$limit = "LIMIT $max_products OFFSET $page";
				$this->db->limit($max_products, $page);
			}
		}
		
		$query = $this->db->get('products');
		$result = $query->result();
		$i = 0;
		foreach ($result as $key => $value) {
			unset($result[$i]->description);
			unset($result[$i]->price);
			unset($result[$i]->is_featured);
			unset($result[$i]->date_featured);
			unset($result[$i]->package_size);
			unset($result[$i]->storage_life);

			// $this->load->model('filter_model');
			$brand = $this->filter_model->getBrand($value->brand_id);
			if ($brand == "") {
				$result[$i]->brand = "N/A";
			}
			else {
				$result[$i]->brand = $brand;
			}
			
			unset($result[$i]->brand_id);

			$result[$i]->sub_cat = $this->getSubCategoryByProduct($value->product_id);
			unset($result[$i]->sub_cat_id);
			$result[$i]->sub_app = $this->getSubApplicationByProduct($value->product_id);
			unset($result[$i++]->sub_app_id);
		}
		return $result;
	}

	public function getProduct($product_id, $user_id = NULL)
	{
		$this->db->where('product_id', $product_id);
		$query = $this->db->get('products');
		$result = null;
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];

			// $this->load->model('filter_model');
			$brand = $this->filter_model->getBrand($result->brand_id);
			if ($brand == "") {
				$result->brand = "N/A";
			}
			else {
				$result->brand = $brand;
			}
			// $result->brand = $this->filter_model->getBrand($result->brand_id);
			unset($result->brand_id);

			$result->is_wishlisted = $this->isWishlisted(array("user_id" => $user_id, "product_id" => $product_id));
			$result->sub_cat = $this->getSubCategoryByProduct($result->product_id);
			unset($result->sub_cat_id);
			$result->sub_app = $this->getSubApplicationByProduct($result->product_id);
			unset($result->sub_app_id);
			
			
		}
		return $result;
	}

	public function getFeaturedProducts($limit)
	{
		$this->db->select('product_id, name, tag_line, price, image, is_featured, date_featured, brand_id');
		$this->db->where('is_featured', 1);
		$this->db->order_by('date_featured', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get('products');
		if ($query->num_rows() >= 1) {
			$result = $query->result();
			return $result;
		}
	}

	public function getFilteredProducts($cat_id, $app_id, $brand_id)
	{
		$filtered_product = array();
		// if ($cat_id != 0) {
		// 	$this->db->from('product_by_sub_cat');
		// 	$this->db->join('products', 'product_by_sub_cat.product_id = products.product_id');
		// 	$this->db->where('sub_cat_id', $cat_id);
		// 	$query = $this->db->get();
		// 	$result = $query->result();
		// 	foreach ($result as $value) {
		// 		unset($value->id);
		// 		unset($value->sub_app_id);
		// 		unset($value->description);
		// 		unset($value->price);
		// 		unset($value->is_featured);
		// 		unset($value->date_featured);
		// 		unset($value->package_size);
		// 		unset($value->storage_life);
		// 		$value->sub_cat = $this->getSubCategoryByProduct($value->product_id);
		// 		$value->sub_app = $this->getSubApplicationByProduct($value->product_id);
		// 		$filtered_product[] = $value;
		// 	}
		// }

		// if ($app_id != 0) {
		// 	$this->db->from('product_by_sub_app');
		// 	$this->db->join('products', 'product_by_sub_app.product_id = products.product_id');
		// 	$this->db->where('sub_app_id', $app_id);
		// 	$query = $this->db->get();
		// 	$result = $query->result();
		// 	foreach ($result as $value) {
		// 		unset($value->id);
		// 		unset($value->sub_app_id);
		// 		unset($value->description);
		// 		unset($value->price);
		// 		unset($value->is_featured);
		// 		unset($value->date_featured);
		// 		unset($value->package_size);
		// 		unset($value->storage_life);
		// 		$value->sub_cat = $this->getSubCategoryByProduct($value->product_id);
		// 		$value->sub_app = $this->getSubApplicationByProduct($value->product_id);
		// 		$filtered_product[] = $value;
		// 	}
		// }

		// if ($brand_id != 0) {
		// 	$this->db->from('products');
		// 	$this->db->where('brand_id', $brand_id);
		// 	$query = $this->db->get();
		// 	$result = $query->result();
		// 	foreach ($result as $value) {
		// 		unset($value->id);
		// 		unset($value->sub_app_id);
		// 		unset($value->description);
		// 		unset($value->price);
		// 		unset($value->is_featured);
		// 		unset($value->date_featured);
		// 		unset($value->package_size);
		// 		unset($value->storage_life);
		// 		$value->sub_cat = $this->getSubCategoryByProduct($value->product_id);
		// 		$value->sub_app = $this->getSubApplicationByProduct($value->product_id);
		// 		$filtered_product[] = $value;
		// 	}
		// }

			$this->db->from('products');
			$this->db->join('product_by_sub_app', 'product_by_sub_app.product_id = products.product_id');
			$this->db->join('product_by_sub_cat', 'product_by_sub_cat.product_id = products.product_id');

			if ($cat_id != 0) {
				$this->db->where('sub_cat_id', $cat_id);
			}

			if ($app_id != 0) {
				$this->db->where('sub_app_id', $app_id);
			}

			if ($brand_id != 0) {
				$this->db->where('brand_id', $brand_id);
			}
			
			$this->db->group_by("products.product_id");
			$query = $this->db->get();
			$result = $query->result();
			foreach ($result as $value) {
				unset($value->id);
				unset($value->sub_app_id);
				unset($value->sub_cat_id);
				unset($value->description);
				unset($value->price);
				unset($value->is_featured);
				unset($value->date_featured);
				unset($value->package_size);
				unset($value->storage_life);
				$value->brand = $this->filter_model->getBrand($value->brand_id);
				unset($value->brand_id);
				$value->sub_cat = $this->getSubCategoryByProduct($value->product_id);
				$value->sub_app = $this->getSubApplicationByProduct($value->product_id);
				$filtered_product[] = $value;
			}
		return $filtered_product;
	}

	public function getSubCategoryByProduct($id)
	{
		$this->db->select('*');
		$this->db->from('product_by_sub_cat');
		$this->db->join('sub_categories', 'sub_categories.sub_cat_id = product_by_sub_cat.sub_cat_id');
		$this->db->where('product_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		$sub_cat = array();
		foreach ($result as $key => $value) {
			$sub_cat[] = $value->name;
		}
		return $sub_cat;
	}

	public function getSubApplicationByProduct($id)
	{
		$this->db->select('*');
		$this->db->from('product_by_sub_app');
		$this->db->join('sub_applications', 'sub_applications.sub_app_id = product_by_sub_app.sub_app_id');
		$this->db->where('product_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		$sub_app = array();
		foreach ($result as $key => $value) {
			$sub_app[] = $value->name;
		}
		return $sub_app;
	}

	public function isWishlisted($post)
	{
		$query = $this->db->get_where('wishlist_product', $post);
		if ($query->num_rows() >= 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public function addToWishlist($post)
	{
		$this->db->insert('wishlist_product', $post);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function deleteFromWishlist($post)
	{
		$this->db->delete('wishlist_product', $post);

		return ($this->db->affected_rows() != 1) ? false : true;
	}
}