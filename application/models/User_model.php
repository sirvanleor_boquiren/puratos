<?php

class User_model extends CI_model {

	public $title;
	public $fname;
	public $lname;
	public $email;
	public $mobile;
	public $password;
	public $address;
	public $company;
	public $birthday;
	public $segment;
	public $account_type;
	public $reg_token;

	public function __construct()
    {
        $this->load->database();
        $this->load->helper('string');
        // $this->load->library('email');
    }

	#Register
	public function insertUser() {
		$this->db->insert('users', $this);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	#forgot password
	public function verifyEmail($post) {
		$this->db->where('email', $post['email']);
		$this->db->where('account_type', 0);
		$query = $this->db->get('users');
		if ($query->num_rows() >= 1) {
			$data = $query->result()[0];

			/* will give new password */
			$new_pass = random_string('alnum', 12);
			if ($this->db->update('users', array('password' => sha1($new_pass)), array('email' => $data->email))) {
				$data_arr['email'] = $data->email;
				$data_arr['new_pass'] = $new_pass;
				return $data_arr;
			}
			/* ––––––––––––––––––––– */

			/* with input new password form */
			// $pass_token = random_string('alnum', 100);
			// if ($this->db->update('users', array('pass_token' => $pass_token), array('email' => $data->email))) {
			// 	$data_arr['email'] = $data->email;
			// 	$data_arr['pass_token'] = $pass_token;
			// 	return $data_arr;
			// }
			/* –––––––––––––––––––––– */
		}
	}

	#Login
	public function verifyCredentials($post) {
		if ($post['account_type'] == 0) {
			$this->db->where("(email = " . $post['username'] . " OR mobile = " . $post['username'] . " OR username = " . $post['username'] . ")");
			$this->db->where('password', sha1($post['password']));
			$this->db->where('account_type', "0");
		}
		else {
			$this->db->where('email', $post['username']);
			$this->db->where('account_type', 1);
		}
		$query = $this->db->get('users');

		if ($query->num_rows() >= 1) {

			$data = $query->result()[0];

			if ($data->is_active == 1) {
				return $this->getUserById($data->user_id);
			}
			else {
				return "401"; #not activated
			}
		}
	}

	public function updateUser($id, $post) {

		if (isset($post['password'])) { #change password
			$post['password'] = sha1($post['password']);
		}

		#check if user is in database
		$this->db->where('user_id', $id);
		$query = $this->db->get('users');
		if ($query->num_rows() == 1) {
			#update user if true
			return $this->db->update('users', $post, array('user_id' => $id));
		}
		else {
			return false;
		}
	}

	public function updateProfilePic($id, $post) {
		return $this->db->update('users', $post, array('user_id' => $id));
	}

	public function getUserById($id) {
		$this->db->where('user_id', $id);
		$query = $this->db->get('users');
		if ($query->num_rows() <= 0) {
			return null;
		}
		$arr_data = $query->result()[0];

		$arr_data->profile_pic = base_url() . "uploads/profile_pic/" . $arr_data->profile_pic;

		unset($arr_data->reg_token);
		unset($arr_data->pass_token);
		unset($arr_data->is_active);
		unset($arr_data->password);

		return $arr_data;
	}

	#setters
	public function setUser($post) {
		if ($post['account_type'] == 0) { # if account type is 0(email) set password and token
			$post['password'] = sha1($post['password']);
			$post['reg_token'] = random_string('alnum', 100);
		}
		else { # if account type is 1(fb) password and token is not required
			$post['password'] = "";
			$post['reg_token'] = "";
			$post['is_active'] = 1;
		}
		

		foreach ($post as $key => $value) {
			$this->$key = $value;
		}

	}
	#getters
	public function getUser(){
		return $this;
	}

	#check if there's null value
	public function haveNull($data) {
		foreach ($data as $property) {
			if ($property == "") {
				return true;
			}
		}
	}

	#check if id is in db
	public function checkUserById($id) {
		$this->db->where('user_id', $id);
		$query = $this->db->get('users');
		return $query->num_rows();
	}

	#check if email is in db
	public function checkUserByEmail($email) {
		$this->db->where('email', $email);
		$query = $this->db->get('users');
		return $query->num_rows();
	}

	#check if mobile number is in db
	public function checkUserByMobile($mobile) {
		$this->db->where('mobile', $mobile);
		$query = $this->db->get('users');
		return $query->num_rows();
	}
	
	#check if username is in db
	public function checkUserByUsername($username) {
		$this->db->where('username', $username);
		$query = $this->db->get('users');
		return $query->num_rows();
	}

	/* Cart module */
	public function getProductsFromCart($id) #user_id
	{
		// $this->db->select('cart_id, product_id, quantity');
		$this->db->where('user_id', $id);
		$query = $this->db->get('cart');
		$result = $query->result();
		$this->load->model('product_model');
		$i = 0;
		foreach ($result as $value) {
			$product_result = $this->product_model->getProduct($value->product_id, $value->user_id);
			if ($product_result != null) {
				$result[$i]->product_name = $product_result->name;
				$result[$i]->product_price = $product_result->price;
				$result[$i]->product_image = $product_result->image;
				unset($result[$i]->user_id);
			}
			else {
				unset($result[$i]);
			}
			$i++;
		}
		return array_values($result);
	}

	public function isAddedToCart($post)
	{
		$query = $this->db->get_where('cart', array('user_id' => $post['user_id'], 'product_id' => $post['product_id']));
		if ($query->num_rows() >= 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public function addToCart($post)
	{
		$this->db->insert('cart', $post);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function updateProductInCart($cart_id, $put)
	{
		return $this->db->update('cart', $put, array('cart_id' => $cart_id));
	}

	public function updateProductsInCart($put)
	{
		$ret = true;
		foreach ($put as $value) {
			foreach ($value as $temp) {
				if(!$this->db->update('cart', array('quantity' => $temp['quantity']), array('user_id' => $temp['user_id'], 'product_id' => $temp['product_id']))) {
					$ret = false;
				}
			}
		}
		return $ret;
	}

	public function deleteFromCart($delete)
	{
		$this->db->delete('cart', $delete);
		return ($this->db->affected_rows() < 1) ? false : true;
	}

	public function getProductFromCart($post) #get 1 product
	{
		$this->db->where('user_id', $post['user_id']);
		$this->db->where('product_id', $post['product_id']);
		$query = $this->db->get('cart');
		return $query->result()[0];
	}
	/* Cart module */

	/* Shipping Address */
	public function getShippingAddress($id) #user_id
	{
		$this->db->where('user_id', $id);
		$query = $this->db->get('shipping_address');
		return $query->result();
	}

	public function addShippingAddress($post)
	{
		$this->db->insert('shipping_address', $post);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function updateShippingAddress($put)
	{
		return $this->db->update('shipping_address', $put, array('shipping_id' => $put['shipping_id']));
	}

	public function deleteShippingAddress($delete)
	{
		$this->db->delete('shipping_address', $delete);
		return ($this->db->affected_rows() < 1) ? false : true;
	}
	/* Shipping Address */

	/* Checkout */
	public function addOrder($post)
	{
		$post['date_purchased'] = date("Y-m-d h:i:s");
		$this->db->insert('order_history', $post);
		return $this->addProductOrder($post, $this->db->insert_id());
	}

	public function addProductOrder($post, $order_id)
	{
		$data['order_id'] = $order_id;
		foreach ($this->getProductsFromCart($post['user_id']) as $product) {
			$data['product_id'] = $product->product_id;
			$data['quantity'] = $product->quantity;
			$this->db->insert('user_order', $data);
		}
		$delete['user_id'] = $post['user_id'];
		unset($data);
		$data['order_id'] = $order_id;
		$data['cartdeleted'] = $this->deleteFromCart($delete);
		return $data;
	}

	public function getOrder($order_id)
	{
		$this->db->from('order_history');
		$this->db->join('user_order', 'order_history.order_id = user_order.order_id');
		$this->db->where('order_history.order_id', $order_id);
		$query = $this->db->get();
		return $query->result();
	}
	/* Checkout */
}