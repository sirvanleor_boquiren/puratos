<?php

class Filter_model extends CI_model {

	public function getCategories()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('categories');
		$result = $query->result();
		$arr_data = array();
		foreach ($result as $key => $value) {
			$value->sub_category = $this->getSubCategories($value->category_id);
			$arr_data[] = $value;
		}
		return($arr_data);
	}

	public function getSubCategories($id)
	{
		$this->db->select('sub_cat_id, name');
		$this->db->where('category_id', $id);
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('sub_categories');
		return $query->result();
	}

	public function getApplications()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('applications');
		$result = $query->result();
		$arr_data = array();
		foreach ($result as $key => $value) {
			$value->sub_application = $this->getSubApplications($value->application_id);
			$arr_data[] = $value;
		}
		return($arr_data);
	}

	public function getSubApplications($id)
	{
		$this->db->select('sub_app_id, name');
		$this->db->where('application_id', $id);
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('sub_applications');
		return $query->result();
	}

	public function getBrands()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('brands');
		return $query->result();
	}

	public function getBrand($id)
	{
		$this->db->select('name');
		$this->db->where('brand_id', $id);
		$query = $this->db->get('brands');
		if ($query->num_rows() >= 1) {
			return $query->result()[0]->name;
		}
		else {
			return NULL;
		}
	}
}