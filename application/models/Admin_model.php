<?php

class Admin_model extends CI_model {

/* Admin */
	public function getAdmin($post)
	{
		$this->db->where('username', $_POST['username']);
		$this->db->where('password', $_POST['password']);
		$query = $this->db->get('admin');
		if ($query->num_rows() >= 1) {
			return $query->result()[0];
		}
		else {
			return false;
		}
	}

	public function getAdmins()
	{
		$query = $this->db->get('admin');
		return $query->result();
	}

	public function addAdmin($post)
	{
		return $this->db->insert('admin', $post);
	}

	public function updateAdmin($post)
	{
		return $this->db->update('admin', $post, array('admin_id' => $_POST['admin_id']));
	}

	public function deleteAdmin($id)
	{
		return $this->db->delete("admin", array('admin_id' => $id));
	}

	public function getAdminById($id)
	{
		$this->db->where('admin_id', $id);
		$query = $this->db->get('admin');
		if ($query->num_rows() >= 1) {
			return $query->result()[0];
		}
		else{
			return false;
		}
	}
/* Admin END */

/* Users */
	public function getUsers()
	{
		$this->db->order_by('user_id', 'DESC');
		$query = $this->db->get('users');
		return $query->result();
	}

	public function updateUser($post)
	{
		return $this->db->update('users', $post, array('user_id' => $_POST['user_id']));
	}

	public function deleteUser($id)
	{
		return $this->db->delete("users", array('user_id' => $id));
	}
/* Users END */

/* Videos */
	public function getVideos()
	{
		$this->db->order_by('video_id', 'DESC');
		$query = $this->db->get('videos');
		return $query->result();
	}

	public function getVideo($id)
	{
		$this->db->where('video_id', $id);
		$query = $this->db->get('videos');
		$arr_data = $query->result()[0];

		return $arr_data;
	}

	public function addVideo($post)
	{
		return $this->db->insert('videos', $post);
	}

	public function updateVideo($post)
	{
		$this->db->where('video_id', $_POST['video_id']);
		return $this->db->update('videos', $post, array('video_id' => $_POST['video_id']));
	}

	public function deleteVideo($id)
	{
		return $this->db->delete("videos", array('video_id' => $id));
	}
/* Videos END */

/* News/Blog */
	public function getNews()
	{
		$this->db->order_by('news_id', 'DESC');
		$query = $this->db->get('news');
		return $query->result();
	}

	public function getNewsById($id)
	{
		$this->db->where('news_id', $id);
		$query = $this->db->get('news');
		return $query->result()[0];
	}

	public function addNews($post)
	{
		return $this->db->insert('news', $post);
	}

	public function updateNews($post)
	{
		$this->db->where('news_id', $_POST['news_id']);
		return $this->db->update('news', $post, array('news_id' => $_POST['news_id']));
	}

	public function deleteNews($id)
	{
		return $this->db->delete("news", array('news_id' => $id));
	}
/* News/Blog END */

/* Products */
	public function getProducts()
	{
		// $query = $this->db->get('products');
		// return $query->result();
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('products');
		$result = $query->result();
		$i = 0;
		foreach ($result as $key => $value) {
			$result[$i]->sub_cat = $this->getSubCategoryByProduct($value->product_id);
			unset($result[$i]->sub_cat_id);
			$result[$i]->sub_app = $this->getSubApplicationByProduct($value->product_id);
			unset($result[$i++]->sub_app_id);
		}
		return $result;
	}

	public function getSubCategoryByProduct($id)
	{
		$this->db->from('product_by_sub_cat');
		$this->db->join('sub_categories', 'sub_categories.sub_cat_id = product_by_sub_cat.sub_cat_id');
		$this->db->where('product_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		$sub_cat = array();
		foreach ($result as $key => $value) {
			$sub_cat[] = $value->name;
		}
		return $sub_cat;
		// return array_column($query->result(), 'name');
	}

	public function getSubApplicationByProduct($id)
	{
		$this->db->from('product_by_sub_app');
		$this->db->join('sub_applications', 'sub_applications.sub_app_id = product_by_sub_app.sub_app_id');
		$this->db->where('product_id', $id);
		$query = $this->db->get();
		$result = $query->result();
		$sub_app = array();
		foreach ($result as $key => $value) {
			$sub_app[] = $value->name;
		}
		return $sub_app;
		// return array_column($query->result(), 'name');
	}

	public function getProductById($id)
	{
		$this->db->where('product_id', $id);
		$query = $this->db->get('products');
		$result = $query->result()[0];

		$result->sub_cat = $this->getSubCategoryByProduct($result->product_id);
		unset($result->sub_cat_id);
		$result->sub_app = $this->getSubApplicationByProduct($result->product_id);
		unset($result->sub_app_id);
		return $result;
	}

	public function addProduct($post)
	{
		if (isset($post['sub_cat_id'])) {
			$sub_cat_id = implode(",", $post['sub_cat_id']);
		}
		else {
			$sub_cat_id = "0";
		}

		if (isset($post['sub_app_id'])) {
			$sub_app_id = implode(",", $post['sub_app_id']);
		}
		else {
			$sub_app_id = "0";
		}
		
		unset($post['sub_cat_id']);
		unset($post['sub_app_id']);

		$this->db->insert('products', $post);
		$product_id = $this->db->insert_id();

		$this->insertProductBySubCat($sub_cat_id, $product_id);
		$this->insertProductBySubApp($sub_app_id, $product_id);

		return $product_id;
		// return true; # TODO
	}

	public function updateProduct($post)
	{
		$this->deleteProductInOtherTable($_POST['product_id'], 'product_by_sub_app');
		$this->deleteProductInOtherTable($_POST['product_id'], 'product_by_sub_cat');

		if (isset($post['sub_cat_id'])) {
			$sub_cat_id = implode(",", $post['sub_cat_id']);
		}
		else {
			$sub_cat_id = "0";
		}
		$this->insertProductBySubCat($sub_cat_id, $_POST['product_id']);

		if (isset($post['sub_app_id'])) {
			$sub_app_id = implode(",", $post['sub_app_id']);
		}
		else {
			$sub_app_id = "0";
		}
		$this->insertProductBySubApp($sub_app_id, $_POST['product_id']);

		unset($post['sub_cat_id']);
		unset($post['sub_app_id']);
		$this->db->where('product_id', $_POST['product_id']);
		return $this->db->update('products', $post);
	}

	public function insertProductBySubCat($cat_ids, $prod_id)
	{
		/* insert product by sub-category */
		$data['product_id'] = $prod_id;
		$post['sub_cat_id'] = explode(",", $cat_ids);
		foreach ($post['sub_cat_id'] as $sub_cat_id) {
			$data['sub_cat_id'] = $sub_cat_id;
			$this->db->insert('product_by_sub_cat', $data);
		}
		/* insert product by sub-category END */
	}

	public function insertProductBySubApp($app_ids, $prod_id)
	{
		/* insert product by sub-application */
		$data['product_id'] = $prod_id;
		$post['sub_app_id'] = explode(",", $app_ids);
		foreach ($post['sub_app_id'] as $sub_app_id) {
			$data['sub_app_id'] = $sub_app_id;
			$this->db->insert('product_by_sub_app', $data);
		}
		/* insert product by sub-application END */
	}

	public function deleteProduct($id)
	{
		$this->deleteProductInOtherTable($id, 'product_by_sub_cat');
		$this->deleteProductInOtherTable($id, 'product_by_sub_app');
		$this->deleteProductInOtherTable($id, 'recipes_products');
		$this->deleteProductInOtherTable($id, 'wishlist_product');
		$this->deleteProductInOtherTable($id, 'cart');

		$this->db->where('product_id', $id);
		return $this->db->delete('products');
	}

	public function deleteProductInOtherTable($product_id, $table)
	{
		$this->db->where('product_id', $product_id);
		$this->db->delete($table);
	}
/* Products END */

/* Category */
	public function getCategories()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('categories');
		return $query->result();
	}

	public function getSubCategories()
	{	
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('sub_categories');
		return $query->result();
	}

	public function getSubCategory($id)
	{
		$this->db->where('sub_cat_id', $id);
		$query = $this->db->get('sub_categories');
		$arr_data = $query->result()[0];
		return $arr_data;
	}
/* Category END */

/* Applications */
	public function getApplications()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('applications');
		return $query->result();
	}

	public function getSubApplications()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('sub_applications');
		return $query->result();
	}

	public function getSubApplication($id)
	{
		$this->db->where('sub_app_id', $id);
		$query = $this->db->get('sub_applications');
		$arr_data = $query->result()[0];
		return $arr_data;
	}
/* Applications END */

/* Branches */
	public function getBranches()
	{
		$this->db->order_by('branch_id', 'DESC');
		$query = $this->db->get('branches');
		return $query->result();
	}

	public function getBranch($id)
	{
		$this->db->where('branch_id', $id);
		$query = $this->db->get('branches');
		$arr_data = $query->result()[0];

		return $arr_data;
	}

	public function addBranch($post)
	{
		$this->db->insert('branches', $post);
	}

	public function updateBranch($post)
	{
		$this->db->where('branch_id', $_POST['branch_id']);
		$this->db->update('branches', $post);
	}

	public function getCountries()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('country');
		return $query->result();
	}

	public function getCountry($id)
	{
		$this->db->where('country_id', $id);
		$query = $this->db->get('country');
		$arr_data = $query->result()[0];
		return $arr_data;
	}

	public function addCountry($post)
	{
		return $this->db->insert('country', $post);
	}

	public function updateCountry($post)
	{
		$this->db->where('country_id', $_POST['country_id']);
		return $this->db->update('country', $post);
	}

	public function deleteBranch($id)
	{
		return $this->db->delete("branches", array('branch_id' => $id));
	}

	public function deleteCountry($id)
	{
		return $this->db->delete("country", array('country_id' => $id));
	}

	public function getSchedule($id)
	{
		$this->db->where('branch_id', $id);
		$query = $this->db->get('branch_schedules');
		return $query->result();
	}

	public function addBranchSchedule($post)
	{
		foreach ($post['days'] as $day) {
			$data['start_time'] = $post['start_time'];
			$data['end_time'] = $post['end_time'];
			$data['day'] = $day;
			$data['branch_id'] = $post['branch_id'];
			$this->db->insert('branch_schedules', $data);
		}
	}

	public function deleteBranchSchedule($id)
	{
		return $this->db->delete("branch_schedules", array('id' => $id));
	}

	public function resetMainBranch($country_id)
	{
		$this->db->where('country_id', $country_id);
		return $this->db->update('branches', array('is_main_branch' => 0));
	}
/* Branches END */

/* Inquiries */
	public function getInquiries()
	{
		$this->load->model('user_model');
		$this->db->order_by('inquiry_id', 'DESC');
		$query = $this->db->get('inquiries');
		$result = $query->result();
		foreach ($result as $key => $value) {
			$user_info = $this->user_model->getUserById($value->user_id);
			if ($user_info == NULL) {
				unset($result[$key]);
				continue;
			}
			$result[$key]->email = $user_info->email;
			$result[$key]->name = $user_info->fname . " " . $user_info->lname;
			$result[$key]->mobile = $user_info->mobile;
		}
		return $result;
	}
/* Inquiries END */

/* Recipes */
	public function getRecipes()
	{
		$this->db->order_by('recipe_id', 'DESC');
		$query = $this->db->get('recipes');
		return $query->result();
	}

	public function getRecipe($id)
	{
		$this->db->where('recipe_id', $id);
		$query = $this->db->get('recipes');
		return $query->result()[0];
	}

	public function addRecipe($post)
	{
		$arr_puratos_ingre = $post['puratos_ingredients'];
		$arr_puratos_amount = $post['puratos_ingre_amount'];
		$arr_other_ingre = $post['other_ingredients'];
		$arr_other_amount = $post['other_amount'];
		unset($post['puratos_ingredients']);
		unset($post['puratos_ingre_amount']);
		unset($post['other_ingredients']);
		unset($post['other_amount']);

		if ($this->db->insert('recipes', $post)) {
			$data['recipe_id'] = $this->db->insert_id();

			/* insert in recipes_products table (puratos ingredients) */
			for ($i=0; $i < count($arr_puratos_ingre); $i++) { 
				if ($arr_puratos_ingre[$i] == "" && $arr_puratos_amount[$i] == "") {
					continue;
				}
				$data['product_id'] = $arr_puratos_ingre[$i];
				$data['amount'] = $arr_puratos_amount[$i];
				$this->db->insert('recipes_products', $data);
			}
			/* insert in recipes_products table (puratos ingredients) END */
			unset($data['product_id']);
			unset($data['amount']);
			/* insert in other_ingredients table */
			for ($i=0; $i < count($arr_other_ingre); $i++) {
				if ($arr_other_ingre[$i] == "" && $arr_other_amount[$i] == "") {
					continue;
				}
				$data['name'] = $arr_other_ingre[$i];
				$data['amount'] = $arr_other_amount[$i];
				$this->db->insert('other_ingredients', $data);
			}
			/* insert in other_ingredients table END */

			return true;
		}
	}

	public function updateRecipe($post)
	{
		$arr_puratos_ingre = $post['puratos_ingredients'];
		$arr_puratos_amount = $post['puratos_ingre_amount'];
		$arr_other_ingre = $post['other_ingredients'];
		$arr_other_amount = $post['other_amount'];
		unset($post['puratos_ingredients']);
		unset($post['puratos_ingre_amount']);
		unset($post['other_ingredients']);
		unset($post['other_amount']);

		$this->db->where('recipe_id', $_POST['recipe_id']);
		if ($this->db->update('recipes', $post)) {
			$data['recipe_id'] = $_POST['recipe_id'];

			/* insert in recipes_products table (puratos ingredients) */
			$this->deleteRecipeInOtherTable($data['recipe_id'], 'recipes_products');
			for ($i=0; $i < count($arr_puratos_ingre); $i++) { 
				if ($arr_puratos_ingre[$i] == "" && $arr_puratos_amount[$i] == "") {
					continue;
				}
				$data['product_id'] = $arr_puratos_ingre[$i];
				$data['amount'] = $arr_puratos_amount[$i];
				$this->db->insert('recipes_products', $data);
			}
			unset($data['product_id']);
			unset($data['amount']);
			/* insert in recipes_products table (puratos ingredients) END */

			/* insert in other_ingredients table */
			$this->deleteRecipeInOtherTable($data['recipe_id'], 'other_ingredients');
			for ($i=0; $i < count($arr_other_ingre); $i++) { 
				if ($arr_other_ingre[$i] == "" && $arr_other_amount[$i] == "") {
					continue;
				}
				$data['name'] = $arr_other_ingre[$i];
				$data['amount'] = $arr_other_amount[$i];
				$this->db->insert('other_ingredients', $data);
			}
			/* insert in other_ingredients table END */
		}
		return true;
	}

	public function deleteRecipe($id)
	{
		$this->deleteRecipeInOtherTable($id, 'recipes_products');
		$this->deleteRecipeInOtherTable($id, 'wishlist_recipe');
		$this->deleteRecipeInOtherTable($id, 'other_ingredients');
		
		return $this->db->delete("recipes", array('recipe_id' => $id));
	}

	public function deleteRecipeInOtherTable($recipe_id, $table)
	{
		$this->db->delete($table, array('recipe_id' => $recipe_id));
	}

	public function getOtherIngredientsByRecipe($recipe_id)
	{
		$this->db->where('recipe_id', $recipe_id);
		$query = $this->db->get('other_ingredients');
		return $query->result();
	}

	public function getIngredientsByRecipe($recipe_id)
	{
		$this->db->where('recipe_id', $recipe_id);
		$query = $this->db->get('recipes_products');
		return $query->result();
	}
/* Recipes END */

/* Configuration (Brands, Categories, Applications) */
	/* Brands */
	public function getBrands()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('brands');
		return $query->result();
	}

	public function getBrand($id)
	{
		$this->db->where('brand_id', $id);
		$query = $this->db->get('brands');
		$arr_data = $query->result()[0];
		return $arr_data;
	}

	public function addBrand($post)
	{
		return $this->db->insert('brands', $post);
	}

	public function updateBrand($post)
	{
		$this->db->where('brand_id', $_POST['brand_id']);
		return $this->db->update('brands', $post);
	}

	public function deleteBrand($id)
	{
		return $this->db->delete("brands", array('brand_id' => $id));
	}
	/* Brands */

	/* Categories */
	public function addCategory($post)
	{
		return $this->db->insert('categories', $post);
	}

	public function addSubCategory($post)
	{
		return $this->db->insert('sub_categories', $post);
	}

	public function updateCategory($post)
	{
		$this->db->where('category_id', $_POST['category_id']);
		return $this->db->update('categories', $post);
	}

	public function updateSubCategory($post)
	{
		$this->db->where('sub_cat_id', $_POST['sub_cat_id']);
		return $this->db->update('sub_categories', $post);
	}

	public function deleteCategory($id)
	{
		return $this->db->delete("categories", array('category_id' => $id));
	}

	public function deleteSubCategory($id)
	{
		return $this->db->delete("sub_categories", array('sub_cat_id' => $id));
	}
	/* Categories */
	
	/* Applications */
	public function addApplication($post)
	{
		return $this->db->insert('applications', $post);
	}

	public function addSubApplication($post)
	{
		return $this->db->insert('sub_applications', $post);
	}

	public function updateApplication($post)
	{
		$this->db->where('application_id', $_POST['application_id']);
		return $this->db->update('applications', $post);
	}

	public function updateSubApplication($post)
	{
		$this->db->where('sub_app_id', $_POST['sub_app_id']);
		return $this->db->update('sub_applications', $post);
	}

	public function deleteApplication($id)
	{
		return $this->db->delete("applications", array('application_id' => $id));
	}

	public function deleteSubApplication($id)
	{
		return $this->db->delete("sub_applications", array('sub_app_id' => $id));
	}
	/* Applications */
/* Configuration END */
}