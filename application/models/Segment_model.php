<?php

class Segment_model extends CI_model 
{
	/* web service */
	public function getSegmentsName() 
	{
		$result = $this->getSegments();
		$segment = array();
		foreach ($result as $key => $value) {
			$segment[] = $value->name;
		}
		return $segment;
	}
	/* web service */

	/* cms */
	public function getSegments() 
	{
		$query = $this->db->get('segments');
		return $query->result();
	}

	public function addSegment($post) 
	{
		return $this->db->insert('segments', $post);
	}

	public function updateSegment($post)
	{
		return $this->db->update('segments', $post, array('segment_id' => $_POST['segment_id']));
	}

	public function deleteSegment($id)
	{
		return $this->db->delete("segments", array('segment_id' => $id));
	}

	/* cms */
}