<?php

class Home_model extends CI_model {

	public function __construct()
    {
    	parent::__construct();
    	$this->load->model('product_model');
    	$this->load->model('news_model');
    }

    function cmp($a, $b)
	{
	    return strcmp($b->date, $a->date);
	}

	public function getWhatsNew()
	{
		$whats_new = array();
		$products = $this->product_model->getFeaturedProducts(5);
		if ($products != NULL) {
			foreach ($products as $product) {
				$product->id = $product->product_id;
				unset($product->product_id);
				$product->title = $product->name;
				unset($product->name);
				$product->thumbnail = $product->image;
				unset($product->image);
				unset($product->tag_line);
				unset($product->price);
				unset($product->is_featured);
				$product->date = $product->date_featured;
				unset($product->date_featured);
				unset($product->brand_id);
				$product->sub_title = "category";
				$product->category = "product";
				$whats_new[] = $product;
			}
		}

		$news = $this->news_model->getFeaturedNews(5);
		if ($news != NULL) {
			foreach ($news as $value) {
				$value->id = $value->news_id;
				unset($value->news_id);
				$value->sub_title = $value->date;
				unset($value->date);
				$value->date = $value->date_featured;
				unset($value->date_featured);
				$value->category = "news/blog";
				$whats_new[] = $value;
			}
		}
		usort($whats_new, array($this, 'cmp'));
		return $whats_new;
	}

	/**
	 * search function - merge searches (product, recipe, videos)
	 * @param  string
	 * @return array
	 */
	public function search($string)
	{
		$arr_data = array_merge($this->search_products($string), $this->search_recipes($string), $this->search_videos($string));
		usort($arr_data, array($this, 'cmp'));
		return $arr_data;
	}

	/**
	 * search products
	 * @param  string
	 * @return array
	 */
	public function search_products($string)
	{
		$this->db->select('products.product_id as id, products.name as name, products.tag_line, products.image, products.date_added as date');
		$this->db->from('products');
		$this->db->join('brands', 'brands.brand_id = products.brand_id');

		$this->db->join('product_by_sub_cat', 'product_by_sub_cat.product_id = products.product_id');
		$this->db->join('sub_categories', 'sub_categories.sub_cat_id = product_by_sub_cat.sub_cat_id');
		$this->db->join('categories', 'categories.category_id = sub_categories.category_id');

		$this->db->join('product_by_sub_app', 'product_by_sub_app.product_id = products.product_id');
		$this->db->join('sub_applications', 'sub_applications.sub_app_id = product_by_sub_app.sub_app_id');
		$this->db->join('applications', 'applications.application_id = sub_applications.application_id');

		$this->db->like('products.name', $string);
		$this->db->or_like('products.description', $string);
		$this->db->or_like('products.tag_line', $string);
		$this->db->or_like('products.package_size', $string);
		$this->db->or_like('products.storage_life', $string);
		$this->db->or_like('products.price', $string);

		$this->db->or_like('sub_categories.name', $string);
		$this->db->or_like('categories.name', $string);

		$this->db->or_like('sub_applications.name', $string);
		$this->db->or_like('applications.name', $string);

		$this->db->group_by("products.product_id");
		$query = $this->db->get();
		$result = $query->result();
		if ($query->num_rows() >= 1) {
			foreach ($result as $key => $value) {
				$result[$key]->category = "product";
			}
		}
		return $result;
	}

	/**
	 * search recipes
	 * @param  string
	 * @return array
	 */
	public function search_recipes($string)
	{
		$this->db->select('recipes.recipe_id as id, recipes.title as name, recipes.tag_line, recipes.image, recipes.date_added as date');
		$this->db->from('recipes');
		$this->db->join('categories', 'categories.category_id = recipes.category_id');
		$this->db->like('recipes.title', $string);
		$this->db->or_like('recipes.tag_line', $string);
		$this->db->or_like('categories.name', $string);
		$query = $this->db->get();
		$result = $query->result();
		if ($query->num_rows() >= 1) {
			foreach ($result as $key => $value) {
				$result[$key]->category = "recipe";
			}
		}
		return $result;
	}

	/**
	 * search videos
	 * @param  string
	 * @return array
	 */
	public function search_videos($string)
	{
		$this->db->select('videos.video_id as id, videos.title as name, videos.tag_line, videos.thumbnail as image, videos.date_added as date');
		$this->db->from('videos');
		$this->db->like('videos.title', $string);
		$this->db->or_like('videos.tag_line', $string);
		$this->db->or_like('videos.description', $string);
		$query = $this->db->get();
		$result = $query->result();
		if ($query->num_rows() >= 1) {
			foreach ($result as $key => $value) {
				$result[$key]->category = "video";
			}
		}
		return $result;
	}
}