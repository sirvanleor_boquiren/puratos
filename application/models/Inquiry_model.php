<?php

class Inquiry_model extends CI_model {

	public $user_id;
	public $message;

	public function insertInquiry() 
	{
		$this->db->insert('inquiries', $this);
		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function setInquiry($post)
	{
		foreach ($post as $key => $value) {
			$this->$key = $value;
		}
	}
}