<?php

class Form_model extends CI_model {

	public function __construct()
    {
        $this->load->database();
        $this->load->helper('string');
        // $this->load->library('email');
    }

	#change pass via email/token
	public function verifyPassToken($token) {
		$this->db->where('pass_token', $token);
		$query = $this->db->get('users');
		if ($query->num_rows() == 1) {
			return true;
		}
		else {
			return false;
		}
	}

	#account activation via token
	public function verifyRegToken($token) {
		$this->db->where('reg_token', $token);
		$query = $this->db->get('users');
		if ($query->num_rows() == 1) {
			if ($this->db->update('users', array('reg_token' => '', 'is_active' => 1), array('reg_token' => $token))) { #update reg_token and is_active in db
				return "Success";
			}
		}
		else {
			return "Account already activated";
		}
	}

	#update password and pass_token
	public function updatePassword($token, $newpass) {
		$newpass = sha1($newpass);
		$data = array('password' => $newpass, 'pass_token' => '');
		return $this->db->update('users', $data, array('pass_token' => $token));
	}
}