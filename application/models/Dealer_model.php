<?php
class Dealer_model extends CI_model {
	public function __construct()
	{

	}

	public function getDealers()
	{
		$this->db->order_by('dealer_id', 'DESC');
		$query = $this->db->get('dealers');
		return $query->result();
	}

	public function addDealer($post) 
	{
		return $this->db->insert('dealers', $post);
	}

	public function updateDealer($post)
	{
		return $this->db->update('dealers', $post, array('dealer_id' => $_POST['dealer_id']));
	}

	public function deleteDealer($id)
	{
		return $this->db->delete("dealers", array('dealer_id' => $id));
	}
}