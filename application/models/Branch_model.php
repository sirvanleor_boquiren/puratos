<?php

class Branch_model extends CI_model {

	public function getCountries()
	{
		$this->db->order_by('name', 'ASC');
		$query = $this->db->get('country');
		return $query->result();
	}

	public function getBranchesByCountry($id)
	{
		$this->db->where('country_id', $id);
		$this->db->where('is_main_branch', 1);
		$query = $this->db->get('branches');
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];
			$result->schedule = $this->getScheduleByBranch($result->branch_id);
			return $result;
		}
		else {
			return false;
		}
	}

	public function getDefaultBranch($country)
	{
		$id = $this->getCountryIdByName($country);

		$this->db->where('country_id', $id);
		$this->db->where('is_main_branch', 1);
		$query = $this->db->get('branches');
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];
			$result->schedule = $this->getScheduleByBranch($result->branch_id);
			return $result;
		}
		else {
			return false;
		}
	}

	public function getScheduleByBranch($id)
	{
		/* get schedule */
		$this->db->where('branch_id', $id);
		$this->db->order_by('day');
		$query = $this->db->get('branch_schedules');
		$result = $query->result();
		$schedule = array();
		foreach ($result as $value) {
			switch ($value->day) {
				case 0: $day = 'Sunday'; break;
				case 1: $day = 'Monday'; break;
				case 2: $day = 'Tuesday'; break;
				case 3: $day = 'Wednesday'; break;
				case 4: $day = 'Thursday'; break;
				case 5: $day = 'Friday'; break;
				case 6: $day = 'Saturday'; break;
			}
			$schedule[] = $day . " " . $value->start_time . " to " . $value->end_time;
		}
		return $schedule;
		/* get schedule */
	}

	public function getCountryIdByName($country)
	{
		$this->db->where('name', $country);
		$query = $this->db->get('country');
		$result = $query->result()[0];
		return $result->country_id;
	}
}