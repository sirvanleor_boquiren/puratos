<?php

class Video_model extends CI_model {

	public function __construct()
    {

    }

    public function getVideos($page)
    {
    	$max_vid = 10;
    	
    	if ($page == 1) {
    		$limit = "LIMIT $max_vid";
    		$page = 0;
    	}
    	else {
    		$page = ($page - 1) * $max_vid;
    		$limit = "LIMIT $max_vid OFFSET $page";
    	}

    	// $this->db->order_by('video_id', 'ASC');
    	$this->db->order_by('is_featured', 'DESC');
    	$this->db->order_by('date_added', 'DESC');
    	$this->db->limit($max_vid, $page);
    	$query = $this->db->get('videos');

    	return $query->result();
    }

    public function getVideo($video_id)
    {
    	$this->db->where('video_id', $video_id);
		$query = $this->db->get('videos');
		$result = null;
		if ($query->num_rows() >= 1) {
			$result = $query->result()[0];
		}
		return $result;
    }

    public function getAllVideos()
    {
    	$query = $this->db->get('videos');
    	return $query->result();
    }

    public function getFeaturedVideos($limit)
    {
    	$this->db->select('video_id, thumbnail, title, tag_line, vid_link, description');
		$this->db->where('is_featured', 1);
		$this->db->order_by('date_featured', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get('videos');
		if ($query->num_rows() >= 1) {
			$result = $query->result();
			return $result;
		}
    }
}