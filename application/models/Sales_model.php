<?php
class Sales_model extends CI_model {
	public function __construct()
	{

	}

	/**
	 * get sales team in a branch
	 * @param  branch_id(int)
	 * @return sales_team(array)
	 */
	public function getSalesTeamByBranch($id)
	{
		$this->db->where('branch_id', $id);
		$query = $this->db->get('sales_team');
		return $query->result();
	}

	/**
	 * get all sales team
	 * @return sales_team(array)
	 */
	public function getSalesTeam()
	{
		$this->db->order_by('sales_id', 'DESC');
		$query = $this->db->get('sales_team');
		return $query->result();
	}

	public function addSales($post) 
	{
		return $this->db->insert('sales_team', $post);
	}

	public function updateSales($post)
	{
		return $this->db->update('sales_team', $post, array('sales_id' => $_POST['sales_id']));
	}

	public function deleteSales($id)
	{
		return $this->db->delete("sales_team", array('sales_id' => $id));
	}
}