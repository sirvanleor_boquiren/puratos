<?php

class News_model extends CI_model {

	public function __construct()
    {

    }

    public function getNews($page)
    {
        $max_news = 10;
        
        if ($page == 1) {
            $limit = "LIMIT $max_news";
            $page = 0;
        }
        else {
            $page = ($page - 1) * $max_news;
            $limit = "LIMIT $max_news OFFSET $page";
        }

        $this->db->order_by('news_id', 'DESC');
        $this->db->limit($max_news, $page);
        $query = $this->db->get('news');

        return $query->result();
    }

    public function getBlog($news_id)
    {
        $this->db->where('news_id', $news_id);
        $query = $this->db->get('news');
        $result = null;
        if ($query->num_rows() >= 1) {
            $result = $query->result()[0];
        }
        return $result;
    }

    public function getAllNews()
    {
    	$query = $this->db->get('news');
        return $query->result();
    }

    public function getFeaturedNews($limit)
    {
        // $this->db->select('news_id, title, thumbnail, date');
        $this->db->where('is_featured', 1);
        $this->db->order_by('date_featured', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get('news');
        if ($query->num_rows() >= 1) {
            $result = $query->result();
            return $result;
        }
    }
}