<?php

class Recipe_model extends CI_model {

	public function getAllRecipes($page = NULL)
	{
		if ($page != NULL) {
			$max_recipe = 10;
			if ($page == 1) {
				$limit = "LIMIT $max_recipe";
				$page = 0;
				$this->db->limit($max_recipe, $page);
			}
			else {
				$page = ($page - 1) * $max_recipe;
				$limit = "LIMIT $max_recipe OFFSET $page";
				$this->db->limit($max_recipe, $page);
			}
		}

		// $this->db->order_by('date_added', 'DESC');
		$query = $this->db->get('recipes');

		$result = $query->result();
		foreach ($result as $key => $value) {
			unset($result[$key]->instructions);
			$result[$key]->category = $this->getCategoryById($value->category_id);
			unset($result[$key]->category_id);
		}
		return $result;
	}

	public function getRecipeById($recipe_id, $user_id)
	{
		$this->db->where('recipe_id', $recipe_id);
		$query = $this->db->get('recipes');
		if (!empty($query->result())) {
			$result = $query->result()[0];
			$result->category = $this->getCategoryById($result->category_id);
			$result->total_ingredients = $this->getTotalPuratosIngredients($recipe_id) + $this->getTotalOtherIngredients($recipe_id);
			$result->is_wishlisted = $this->isWishlisted(array("user_id" => $user_id, "recipe_id" => $recipe_id));
			unset($result->category_id);
			return $result;
		}
		else {
			return false;
		}
		
	}

	public function getTotalPuratosIngredients($id)
	{
		$this->db->where('recipe_id', $id);
		$query = $this->db->get('recipes_products');
		return $query->num_rows();
	}

	public function getTotalOtherIngredients($id)
	{
		$this->db->where('recipe_id', $id);
		$query = $this->db->get('other_ingredients');
		return $query->num_rows();
	}

	public function getCategoryById($id) #category_id
	{
		$this->db->where('category_id', $id);
		$query = $this->db->get('categories');
		if ($query->num_rows() >= 1) {
			return $query->result()[0]->name;
		}
		else {
			return "";
		}
		
	}

	public function getPuratosIngredients($id) #recipe_id
	{
		$this->db->select('products.product_id, amount, name, price, image');
		$this->db->from('recipes_products');
		$this->db->join('products', 'recipes_products.product_id = products.product_id');
		$this->db->where('recipe_id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getOtherIngredients($id) #recipe_id
	{
		$this->db->where('recipe_id', $id);
		$query = $this->db->get('other_ingredients');
		$result = $query->result();
		foreach ($result as $key => $value) {
			unset($result[$key]->id);
			unset($result[$key]->recipe_id);
		}
		return $result;
	}

	public function isWishlisted($post)
	{
		$query = $this->db->get_where('wishlist_recipe', $post);
		if ($query->num_rows() >= 1) {
			return true;
		}
		else {
			return false;
		}
	}

	public function addToWishlist($post)
	{
		$this->db->insert('wishlist_recipe', $post);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function deleteFromWishlist($post)
	{
		$this->db->delete('wishlist_recipe', $post);

		return ($this->db->affected_rows() != 1) ? false : true;
	}

	public function getFeaturedRecipes($limit)
	{
		$this->db->select('recipe_id, title, category_id, image');
		$this->db->where('is_featured', 1);
		$this->db->order_by('date_featured', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get('recipes');
		if ($query->num_rows() >= 1) {
			$result = $query->result();
			foreach ($result as $key => $value) {
				$result[$key]->category = $this->getCategoryById($value->category_id);
				unset($result[$key]->category_id);
			}
			return $result;
		}
	}

	/* used in Product controller */
	public function getRecipesByProduct($ids) 
	{
		$this->db->from('recipes_products');
		$this->db->where_in('product_id', $ids);
		$this->db->group_by('recipe_id');
		$query = $this->db->get();
		// $this->db->where('product_id', $id);
		// $query = $this->db->get('recipes_products');
		$result = $query->result();
		$recipe = array();
		foreach ($result as $key => $value) {
			$recipe[] = $this->getRecipe($value->recipe_id);
		}
		return $recipe;
	}

	public function getRecipe($id)
	{
		$this->db->where('recipe_id', $id);
		$query = $this->db->get('recipes');
		$result = $query->result()[0];
		unset($result->instructions);
		$result->category = $this->getCategoryById($result->category_id);
		unset($result->category_id);
		unset($result->servings);
		return $result;
	}
	/* used in Product controller */
}