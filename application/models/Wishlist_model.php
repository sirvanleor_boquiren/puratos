<?php

class Wishlist_model extends CI_model {
	public function getProductWishlist($user_id = NULL, $page = NULL)
	{
		if ($page != NULL) {
			$max_product = 10;
			if ($page == 1) {
				$limit = "LIMIT $max_product";
				$page = 0;
				$this->db->limit($max_product, $page);
			}
			else {
				$page = ($page - 1) * $max_product;
				$limit = "LIMIT $max_product OFFSET $page";
				$this->db->limit($max_product, $page);
			}
		}
		$this->db->select('products.product_id, name, price, image');
		$this->db->from('wishlist_product');
		$this->db->join('products', 'products.product_id = wishlist_product.product_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getRecipeWishlist($user_id = NULL, $page = NULL)
	{
		if ($page != NULL) {
			$max_recipe = 10;
			if ($page == 1) {
				$limit = "LIMIT $max_recipe";
				$page = 0;
				$this->db->limit($max_recipe, $page);
			}
			else {
				$page = ($page - 1) * $max_recipe;
				$limit = "LIMIT $max_recipe OFFSET $page";
				$this->db->limit($max_recipe, $page);
			}
		}

		$this->db->select('recipes.recipe_id, title, categories.name as category, tag_line, image');
		$this->db->from('wishlist_recipe');
		$this->db->join('recipes', 'recipes.recipe_id = wishlist_recipe.recipe_id');
		$this->db->join('categories', 'categories.category_id = recipes.category_id');
		$this->db->where('user_id', $user_id);
		$query = $this->db->get();
		return $query->result();
	}
}