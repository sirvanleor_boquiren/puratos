<?php

class Form extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('form_model');
    }

	public function index() {
	}

	#activate user from mail
    function activate($token) {
        $data['msg'] = $this->form_model->verifyRegToken($token);
        $this->load->view('email_pages/activated', $data);
    }

    #change password from mail
    function newpassword($token) {
    	if ($this->form_model->verifyPassToken($token)) {
	    	$this->form_validation->set_rules('password', 'Password', 'required|min_length[8]',
	                array('required' => 'You must provide a %s.')
	        );
	        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]|min_length[8]');
			$data['msg'] = $this->form_validation->run();
			if ($data['msg'] == true) {
				#change pass in db
				if ($this->form_model->updatePassword($token, $_POST['password'])) {
					$this->load->view('email_pages/changepass_success');
				}
			}
			else {
				$this->load->view('email_pages/changepass', $data);
			}
		}
		else {
			$this->load->view('email_pages/invalid_link');
		}
    }
}