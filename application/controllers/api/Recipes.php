<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Recipes extends REST_Controller {

    #GET api/recipes
    #GET api/recipes/recipe/{recipe_id}/{user_id}
    #POST api/recipes/wishlist

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('recipe_model');
        $this->load->model('home_model');
    }

    public function index_get()
    {
        $page = NULL;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $result = $this->recipe_model->getAllRecipes($page);
        // if ($result) {
            $data['status'] = 200;
            $data['recipe'] = $result;
        // }
        // else {
        //     $data['status'] = 400;
        //     $data['data'] = array('message' => 'No Recipe Found');
        // }
        
        $this->response($data);
    }

    public function recipe_get($recipe_id = NULL, $user_id = NULL)
    {
        $data['status'] = 200;
        $data['recipe'] = $this->recipe_model->getRecipeById($recipe_id, $user_id);
        $data['puratos_ingredients'] = $this->recipe_model->getPuratosIngredients($recipe_id);
        $data['other_ingredients'] = $this->recipe_model->getOtherIngredients($recipe_id);
        if (empty($data['recipe'])) {
            $this->response(array(
                'status' => 200,
                'data' => array('message' => 'No Content')
                ));
        }
        else {
            $this->response($data);
        }
    }

    public function recipes_get($ids)
    {
        $ids = explode(",", urldecode($ids));

        $data['status'] = 200;
        $data['recipes'] = $this->recipe_model->getRecipesByProduct($ids);

        $this->response($data);
    }

    public function wishlist_post()
    {
        if ($this->recipe_model->isWishlisted($this->post())) {
            if ($this->recipe_model->deleteFromWishlist($this->post())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Removed from Favorites', 'is_wishlisted' => false)
                    ));
            }
        }
        else {
            if ($this->recipe_model->addToWishlist($this->post())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Added to Favorites', 'is_wishlisted' => true)
                    ));
            }
        }
    }

    public function search_get($string = null)
    {
        $data['status'] = 200;
        $data['search_result'] = $this->home_model->search_recipes($string);
        $this->response($data);
    }
}