<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class News extends REST_Controller {

    #GET api/news

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('news_model');
    }

    public function index_get()
    {
    	if (isset($_GET['page'])) {
            $data['status'] = 200;
            $data['news_blog'] = $this->news_model->getNews($_GET['page']);
            $this->response($data);
        }
        else {
            $data['status'] = 200;
            $data['news_blog'] = $this->news_model->getAllNews();
            $this->response($data);
        }
    }

    public function blog_get($news_id)
    {
        $data['status'] = 200;
        $data['blog'] = $this->news_model->getBlog($news_id);
        if (is_null($data['blog'])) {
            $this->response(array(
                'status' => 200,
                'data' => array('message' => 'No Content')
                ));
        }
        else {
            $this->response($data);
        }
    }
}