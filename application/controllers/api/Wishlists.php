<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Wishlists extends REST_Controller {

    #GET api/wishlists/{user_id}

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('wishlist_model');
    }

    public function index_get($user_id)
    {
        $data['status'] = 200;

        $data['product'] = $this->wishlist_model->getProductWishlist($user_id);
        // if ($data['product'] == NULL) {
        //     $data['product'] = array('message' => 'No product found.');
        // }

        $data['recipe'] = $this->wishlist_model->getRecipeWishlist($user_id);
        // if ($data['recipe'] == NULL) {
        //     $data['recipe'] = array('message' => 'No recipe found.');
        // }
        $this->response($data);
    }

    public function product_get($user_id = NULL)
    {
        $page = NULL;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $data['status'] = 200;
        $data['product'] = $this->wishlist_model->getProductWishlist($user_id, $page);
        // if ($data['product'] == NULL) {
        //     $data['product'] = array('message' => 'No product found.');
        // }
        $this->response($data);
    }

    public function recipe_get($user_id = NULL)
    {  
        $page = NULL;
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        }
        $data['status'] = 200;
        $data['recipe'] = $this->wishlist_model->getRecipeWishlist($user_id, $page);
        // if ($data['recipe'] == NULL) {
        //     $data['recipe'] = array('message' => 'No recipe found.');
        // }
        $this->response($data);
    }
}