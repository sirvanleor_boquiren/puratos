<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Dealers extends REST_Controller {

    #GET api/dealers/

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('dealer_model');
        $this->load->model('sales_model');
    }

    public function index_get()
    {
        $data['status'] = 200;
        $data['dealer'] = $this->dealer_model->getDealers();
        $data['sales_team'] = $this->sales_model->getSalesTeam();

        # unset unnecessary params
        foreach ($data['sales_team'] as $key => $sales_team) {
            unset($data['sales_team'][$key]->sales_id);
            unset($data['sales_team'][$key]->image);
            unset($data['sales_team'][$key]->branch_id);
        }

        $this->response($data);
    }
}