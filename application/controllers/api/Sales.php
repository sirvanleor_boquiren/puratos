<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Sales extends REST_Controller {

    #GET api/sales/branch/{id}

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('sales_model');
    }

    public function index_get()
    {
        
    }

    public function branch_get($id)
    {
        $data['status'] = 200;
        $data['sales_team'] = $this->sales_model->getSalesTeamByBranch($id);
        $this->response($data);
    }
}