<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Branches extends REST_Controller {

    #GET api/branches/countries
    #GET api/branches/{country_id}


	public function __construct() 
	{
        parent::__construct();
        $this->load->model('branch_model');
    }

    public function index_get($id)
    {
        if ($this->branch_model->getBranchesByCountry($id)) {
            $data['status'] = 200;
            $data['branch'] = $this->branch_model->getBranchesByCountry($id);
        }
        else {
            $data = array('status' => 400, 'data' => array('message' => 'No Branch in this country.'));
        }
        
        $this->response($data);
    }

    public function countries_get()
    {
        $data['status'] = 200;
        $data['countries'] = $this->branch_model->getCountries();
        $this->response($data);
    }

    public function default_get($country = "Philippines")
    {
        if ($this->branch_model->getDefaultBranch($country)) {
            $data['status'] = 200;
            $data['branch'] = $this->branch_model->getDefaultBranch($country);
        }
        else {
            $data = array('status' => 400, 'data' => array('message' => 'No Branch in this country.'));
        }
        
        $this->response($data);
    }
}