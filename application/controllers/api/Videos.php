<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Videos extends REST_Controller {

	#GET api/videos

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('video_model');
    }

    public function index_get()
    {
    	if (isset($_GET['page'])) {
    		$data['status'] = 200;
    		$data['videos'] = $this->video_model->getVideos($_GET['page']);
    		$this->response($data);
    	}
    	else {
    		$data['status'] = 200;
    		$data['videos'] = $this->video_model->getAllVideos();
    		$this->response($data);
    	}
    }

    public function video_get($video_id = NULL)
    {
    	$data['status'] = 200;
		$data['video'] = $this->video_model->getVideo($video_id);
		if (is_null($data['video'])) {
			$this->response(array(
                'status' => 200,
                'data' => array('message' => 'No Content')
                ));
		}
		else {
			$this->response($data);
		}
    }
}