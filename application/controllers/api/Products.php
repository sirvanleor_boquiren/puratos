<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Products extends REST_Controller {

	#GET api/products
	#GET api/products/product/{product_id}/{user_id}
	#GET api/products/featured
	#GET api/products/filtered/cat/{id}/app/{id}
	#
	#POST api/products/wishlist

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('product_model');
        $this->load->model('recipe_model');
        $this->load->model('home_model');
    }

	#get all list of products
	public function index_get()
	{
		$data['status'] = 200;
		if (isset($_GET['page'])) {
			$data['products'] = $this->product_model->getProducts($_GET['page']);
		}
		else {
			$data['products'] = $this->product_model->getProducts();
		}
		// if (empty($data['products'])) {
		// 	$this->response(array(
  //               'status' => 200,
  //               'data' => array('message' => 'No Content')
  //               ));
		// }
		// else {
		// 	$this->response($data);
		// }
		$this->response($data);
		
	}

	#product details
	public function product_get($product_id = NULL, $user_id = NULL)
	{
		$data['status'] = 200;
		$data['product'] = $this->product_model->getProduct($product_id, $user_id);
		$data['recipe'] = $this->recipe_model->getRecipesByProduct(array($product_id));
		if (is_null($data['product'])) {
			$this->response(array(
                'status' => 200,
                'data' => array('message' => 'No Content')
                ));
		}
		else {
			$this->response($data);
		}
	}

	#get 3 featured products
	public function featured_get()
	{
		$data['status'] = 200;
		$data['featured_products'] = $this->product_model->getFeaturedProducts(3);
		if (is_null($data['featured_products'])) {
			$this->response(array(
                'status' => 200,
                'data' => array('message' => 'No Content')
                ));
		}
		else {
			$this->response($data);
		}
	}

	public function filtered_get()
	{
		if ($this->get('cat') === null) {
			$cat_id = 0;
		}
		else {
			$cat_id = $this->get('cat');
		}

		if ($this->get('app') === null) {
			$app_id = 0;
		}
		else {
			$app_id = $this->get('app');
		}

		if ($this->get('brand') === null) {
			$brand_id = 0;
		}
		else {
			$brand_id = $this->get('brand');
		}

		$data['status'] = 200;
		$data['products'] = $this->product_model->getFilteredProducts($cat_id, $app_id, $brand_id);
		// if (empty($data['products'])) {
		// 	$this->response(array(
  //               'status' => 200,
  //               'data' => array('message' => 'No Content')
  //               ));
		// }
		// else {
		// 	$this->response($data);
		// }
		$this->response($data);
	}

	public function wishlist_post()
    {
        if ($this->product_model->isWishlisted($this->post())) {
            if ($this->product_model->deleteFromWishlist($this->post())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Removed from Favorites', 'is_wishlisted' => false)
                    ));
            }
        }
        else {
            if ($this->product_model->addToWishlist($this->post())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Added to Favorites', 'is_wishlisted' => true)
                    ));
            }
        }
    }

    public function search_get($string = null)
    {
        $data['status'] = 200;
        $data['search_result'] = $this->home_model->search_products($string);
        $this->response($data);
    }
}