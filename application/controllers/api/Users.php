<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Users extends REST_Controller {
    
    #POST api/users/login
    #POST api/users/register
    #PUT api/users/user/{id}
    #GET api/users/user/{id}

    /* Cart */
    #POST api/users/cart
    #PUT api/users/cart/
    #DELETE api/users/cart
    #GET api/users/cart/user/{user_id}

    #GET api/users/
    #DELETE api/users/user/{id}

    public function __construct() {
        parent::__construct();

        // models
        $this->load->model('user_model');
        $this->load->model('product_model');

        $this->load->library('email');
        $config_mail['protocol']='smtp';
        $config_mail['smtp_host']='mail.smtp2go.com';
        $config_mail['smtp_port']='80';
        $config_mail['smtp_timeout']='30';
        $config_mail['smtp_user']='betamail@optimindsolutions.com';
        $config_mail['smtp_pass']='MDFldDJ5a3lkbWk3';
        $config_mail['charset']='utf-8';
        $config_mail['newline']="\r\n";
        $config_mail['wordwrap'] = TRUE;
        $config_mail['mailtype'] = 'html';
        $this->email->initialize($config_mail);
    }

    public function index_get()
    {
        // var_dump($this->input->request_headers()['Country']);
        // die();
    }

    function user_get($id)
    {
        $data = $this->user_model->getUserById($id);
        $this->response($data);
    }

    #forgot password
    function forgotpassword_post() {
        $data = $this->user_model->verifyEmail($this->post());
        if (count($data)) {
            $this->email->from('noreply@puratos.com', 'Puratos');
            $this->email->to($data['email']);
            $this->email->subject('Puratos - Forgot Password');

            /* will give new password */
            $msg = "
            <p>Someone has requested a password reset for username: ".$data['email']."</p>
            <p>Your new password is: ".$data['new_pass']."</p>
            ";
            /* ––––––––––––––––––––– */

            /* with input new password form */
            // $msg = "
            // <p>Someone has requested a password reset for username: ".$data['email']."</p>
            // <p>If this was a mistake, just ignore this email and nothing will happen.</p>
            // <p>To reset your password, visit the following address:</p>
            // <p>".base_url()."form/newpassword/".$data['pass_token']."</p>
            // ";
            /* –––––––––––––––––––––– */

            $this->email->message($msg);
            $this->email->send();

            $this->response(array(
                'status' => 200,
                'data' => array('message' => 'Success')
                ));
        }
        else {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Invalid email address')
                ));
        }
    }

    #login
    function login_post()
    {
        if (($this->post('username') != "" && $this->post('password') != "" && $this->post('account_type') == 0) || ($this->post('username') != "" && $this->post('account_type') == 1)) {
           $login = $this->user_model->verifyCredentials($this->post());
            if ($login == "401") {
                $data['status'] = 400;
                $data['data'] = array('message' => "Not yet Activated");
            }
            elseif ($login != null) {
                $data['status'] = 200;
                $data['profile'] = $login;
            }
            else {
                $data['status'] = 400;
                $data['data'] = array('message' => "Incorrect username/password");
            }
        }
        else {
            $data['status'] = 400;
            $data['data'] = array('message' => "Incorrect username/password");
        }
        
        $this->response($data);
    }

    #create new user
    function register_post()
    {
        #check user if already registered
        if ($this->user_model->checkUserByEmail($this->post('email'))) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Email address already in use.')
                ));
            return;
        }

        #check user if already registered (mobile)
        if ($this->user_model->checkUserByMobile($this->post('mobile')) && $this->post('account_type') == 0) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Mobile already in use.')
                ));
            return;
        }

        #check user if already registered (username)
        if ($this->user_model->checkUserByUsername($this->post('username')) && $this->post('account_type') == 0) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Username already in use.')
                ));
            return;
        }

        $this->user_model->setUser($this->post());

        if ($this->user_model->insertUser()) {
            $user = $this->user_model->getUser();

            if ($user->account_type == 0) { # will only send email verification if account type is 0(email)
                $this->email->from('noreply@puratos.com', 'Puratos');
                $this->email->to($user->email);

                $this->email->subject('Puratos - Registration');
                $msg = "
                <p>Registration Successful!</p>
                <p>To activate your account please click the link below.</p>
                <p>".base_url()."form/activate/".$user->reg_token."</p>
                ";
                $this->email->message($msg);  

                $this->email->send();
            }

            $this->response(array(
                'status' => 201, 
                'data' => array('message' => "Success")
                ));
        }
        else {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => "Missing Data")
                ));
        }
    }

    #Update user
    function user_put($id)
    {
        if ($this->user_model->haveNull($this->put())) {
            #if have NULL value
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Missing Data')
                ));
        }
        else {
            if ($this->user_model->updateUser($id, $this->put())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Success'),
                    'profile' => $this->user_model->getUserById($id)
                    ));
            }
            else {
                #user not found in database
                $this->response(array(
                    'status' => 400,
                    'data' => array('message' => "User not found")
                    ));
            }
        }
    }

    #change profile pic
    function changeprofilepic_post($id)
    {
        #check id if not null
        if ($this->user_model->checkUserById($id) == 0) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'User not found')
                ));
        }
        else {
            #store file in folder
            $new_filename = time();
            $config['upload_path'] = "./uploads/profile_pic/";
            $config['allowed_types'] = 'gif|jpg|jpeg|png';
            $config['file_name'] = $new_filename;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload('profile_picture')) {
                #store file in db
                $_POST['profile_pic'] = $new_filename . $this->upload->data('file_ext');
                if ($this->user_model->updateProfilePic($id, $this->input->post())) {
                    $this->response(array(
                        'status' => 200,
                        'data' => array('message' => 'Success'),
                        'profile' => $this->user_model->getUserById($id)
                        ));
                }
            }
            else {
                $error = array('error' => $this->upload->display_errors());
                $this->response(array(
                    'status' => 400,
                    'data' => array('message' => $error)
                    ));
            }
        }
    }
 
    function user_delete($id)
    {
        $data = array('returned: '. $id);
        $this->response($data);
    }

/* Add to cart */
    public function cart_get()
    {
        $data['status'] = 200;
        $data['cart'] = $this->user_model->getProductsFromCart($this->get('user'));
        $this->response($data);
    }

    public function cart_post()
    {
        if (empty($this->post()) || $this->user_model->haveNull($this->post())) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Bad request')
                ));
        }
        else {
            if ($this->user_model->isAddedToCart($this->post())) {
                $productFromCart = $this->user_model->getProductFromCart($this->post());
                $put = array('quantity' => $this->post('quantity') + $productFromCart->quantity);
                if ($this->user_model->updateProductInCart($productFromCart->cart_id, $put)) {
                    $this->response(array(
                        'status' => 200,
                        'data' => array('message' => 'Added to cart', 'is_inCart' => true)
                        ));
                }
            }
            else {
                if ($this->user_model->addToCart($this->post())) {
                    $this->response(array(
                        'status' => 200,
                        'data' => array('message' => 'Added to cart', 'is_inCart' => true)
                        ));
                }
            }
        }
        
    }

    public function cart_put()
    {
        if ($this->user_model->updateProductsInCart($this->put()))
        {
            $this->response(array(
                'status' => 200,
                'data' => array('message' => 'Cart updated')
                ));
        }
        else {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Bad request')
                ));
        }
    }

    public function cart_delete()
    {
        $this->deleteItemFromCart($this->delete());
    }

    public function deleteitem_post()
    {
        $this->deleteItemFromCart($this->post());
    }

    public function deleteItemFromCart($post)
    {
        if (empty($post) || $this->user_model->haveNull($post)) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Bad request')
                ));
        }
        else {
            if ($this->user_model->deleteFromCart($post)) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Deleted from cart', 'is_inCart' => false)
                    ));
            }
            else {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'No Content')
                    ));
            }
        }
    }
/* Add to cart END */

/* Shipping address */
    public function shippingaddress_get()
    {
        $data['status'] = 200;
        $data['shipping_address'] = $this->user_model->getShippingAddress($this->get('user'));
        $this->response($data);
    }

    public function shippingaddress_post()
    {
        # address_line2 not required field
        $arr_data = $this->post();
        unset($arr_data['address_line2']);
        ##

        if (empty($this->post()) || $this->user_model->haveNull($arr_data)) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'No Content')
                ));
        }
        else {
            if ($this->user_model->addShippingAddress($this->post())) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Shipping address added')
                    ));
            }
            else {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Failed to add address')
                    ));
            }
        }
    }

    public function shippingaddress_put()
    {

        # address_line2 not required field
        $arr_data = $this->put();
        unset($arr_data['address_line2']);
        ##

        if (empty($this->put()) || $this->user_model->haveNull($arr_data)) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'No Content')
                ));
        }
        else {
            if ($this->user_model->updateShippingAddress($this->put()))
            {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Shipping address updated')
                    ));
            }
            else {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Failed to update address')
                    ));
            }
        }
    }

    public function shippingaddress_delete($id = NULL)
    {
        $this->deleteShippingAddress($id);
    }

    public function deleteaddress_post($id = NULL)
    {
        $this->deleteShippingAddress($id);
    }

    public function deleteShippingAddress($id)
    {
        if ($id == NULL) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'Missing shipping_id')
                ));
        }
        else {
            $delete['shipping_id'] = $id;
            if ($this->user_model->deleteShippingAddress($delete)) {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Shipping address deleted')
                    ));
            }
            else {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'No Content')
                    ));
            }
        }
    }
/* Shipping address END */

/* Checkout */
    public function checkout_post()
    {
        #check if cart is not empty
        if( empty($this->user_model->getProductsFromCart($this->post('user_id'))) )
        {
            $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Cart is empty')
                    ));
        }
        else {
            $data = $this->user_model->addOrder($this->post());
            if ($data['cartdeleted']) {

                /* order email */
                $user = $this->user_model->getUserById($this->post('user_id'));
                $order = $this->user_model->getOrder($data['order_id']);
                $this->email->from('noreply@puratos.com', 'Puratos');
                $this->email->to($user->email);
                $this->email->subject('Puratos - Order No. ' . str_pad($data['order_id'], 8, "0", STR_PAD_LEFT));
                $row_order = "";
                foreach ($order as $value) {
                    $product = $this->product_model->getProduct($value->product_id);
                    $row_order .= "
                    <tr>
                        <td style='border: 1px solid black;'>" . $product->name . "</td>
                        <td style='border: 1px solid black;'>" . $value->quantity . "</td>
                        <td style='border: 1px solid black;'>" . number_format($product->price, 2) . "</td>
                        <td style='border: 1px solid black;'>" . number_format($value->quantity * $product->price, 2) . "</td>
                    </tr>";
                }
                $msg = "
                <table>
                    <tr>
                        <td>Name: " . $user->fname . " " . $user->lname . "</td>
                        <td>Checkout Date: " . date("F d, Y", strtotime($order[0]->date_purchased)) . "</td>
                    </tr>
                    <tr>
                        <td colspan='2'></td>
                    </tr>
                    <tr>
                        <td colspan='2'>
                            <table style='border: 1px solid black; border-collapse: collapse; width: 100%;'>
                                <tr style='background-color: #65dd5a;'>
                                    <th style='border: 1px solid black; width: 100px;'>Product Name</th>
                                    <th style='border: 1px solid black; width: 100px;'>Quantity</th>
                                    <th style='border: 1px solid black; width: 100px;'>Price</th>
                                    <th style='border: 1px solid black; width: 100px;'>Sub Total</th>
                                </tr>
                                $row_order
                            </table>
                        </td>
                    </tr>
                </table>
                ";
                $this->email->message($msg);
                $this->email->send();
                /* order email */

                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Checkout successful')
                    ));
            }
            else {
                $this->response(array(
                    'status' => 200,
                    'data' => array('message' => 'Checkout failed')
                    ));
            }
        }
    }

    public function test_get($order_id)
    {
        $this->user_model->getOrder($order_id);
    }
/* Checkout END */
}
