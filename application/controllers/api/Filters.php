<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Filters extends REST_Controller {

    #GET api/filters/categories
    #GET api/filters/applications
    #GET api/filters/brands


	public function __construct() 
	{
        parent::__construct();
        $this->load->model('filter_model');
    }

    public function categories_get()
    {
        $data['status'] = 200;
        $data['categories'] = $this->filter_model->getCategories();
        $this->response($data);
    }

    public function applications_get()
    {
        $data['status'] = 200;
        $data['applications'] = $this->filter_model->getApplications();
        $this->response($data);
    }

    public function brands_get()
    {
        $data['status'] = 200;
        $data['brands'] = $this->filter_model->getBrands();
        $this->response($data);
    }
}