<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Inquiries extends REST_Controller {

    #POST api/inquiries

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('inquiry_model');
        $this->load->model('user_model');
    }

    public function index_post()
    {
    	#check id if not null
        if ($this->user_model->checkUserById($this->input->post('user_id')) == 0) {
            $this->response(array(
                'status' => 400,
                'data' => array('message' => 'User not found')
                ));
        }
        elseif ($this->input->post('message') == "") {
        	$this->response(array(
                'status' => 400,
                'data' => array('message' => 'Message is empty.')
                ));
        }
        else {
        	$this->inquiry_model->setInquiry($this->post());
        	if ($this->inquiry_model->insertInquiry()) {
        		#TODO send email to admin
        		$this->response(array(
	                'status' => 200,
	                'data' => array('message' => 'Inquiry sent.')
	                ));
        	}
        	else {
        		$this->response(array(
	                'status' => 400,
	                'data' => array('message' => 'Message is empty.')
	                ));
        	}
        }
    }
}