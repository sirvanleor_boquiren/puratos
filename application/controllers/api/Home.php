<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Home extends REST_Controller {

    #GET api/home/

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('video_model');
        $this->load->model('home_model');
        $this->load->model('recipe_model');
    }

    public function index_get()
    {
        $data['status'] = 200;
        $data['videos'] = $this->video_model->getFeaturedVideos(3);
        $data['whats_new'] = $this->home_model->getWhatsNew(10);
        $data['recipes'] = $this->recipe_model->getFeaturedRecipes(10);
        $this->response($data);
    }
}