<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $dynamic_table;
	public $time_picker;

	public function __construct() 
	{
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->model('segment_model');
        $this->load->model('filter_model');
        $this->load->model('dealer_model');
        $this->load->model('sales_model');
        $this->dynamic_table = false;
        $this->time_picker = false;
        $this->multiple_select = false;
    }

	public function wrapper($body, $data = NULL) 
	{
		if (isset($this->session->userdata['logged_in'])) {
			$include['dynamic_table'] = $this->dynamic_table;
			$include['time_picker'] = $this->time_picker;
			$include['multiple_select'] = $this->multiple_select;
			$this->load->view('partials/header', $include);
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			// $this->load->view('partials/right-sidebar');
			$this->load->view('partials/footer', $include);
		}
		else {
			redirect(base_url());
		}
	}
	
	public function index() 
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->products();
		}
		else {
			$this->load->view('login');
		}
	}

	public function login()
	{
		$data['admin'] = $this->admin_model->getAdmin($this->input->post());
		if ($data['admin']) {
			$this->session->userdata['logged_in'] = 1;
			$this->session->userdata['admin_id'] = $data['admin']->admin_id;
			$this->session->userdata['username'] = $data['admin']->username;
			$this->session->userdata['email'] = $data['admin']->email;
			$this->session->userdata['fname'] = $data['admin']->fname;
			$this->session->userdata['lname'] = $data['admin']->lname;
			$this->session->userdata['admin_level'] = $data['admin']->admin_level;
			$this->session->userdata['created_at'] = $data['admin']->created_at;
			redirect(base_url());
		}
		else {
			$msg_data = array('alert_msg' => 'Incorrect Username/Password', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
			$this->load->view('login');
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url());
	}

/* Admin */
	public function admin()
	{
		$this->dynamic_table = true;
		$data['admins'] = $this->admin_model->getAdmins();
		$this->wrapper('admin', $data);
	}

	public function addAdmin()
	{
		if ($this->admin_model->addAdmin($this->input->post())) {
			$msg_data = array('alert_msg' => 'Admin Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add admin', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/admin');
	}

	public function updateAdmin()
	{
		if ($this->admin_model->updateAdmin($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/admin');
	}

	public function changePassword()
	{
		$old_pass = $this->admin_model->getAdminById($_POST['admin_id']);
		// var_dump($old_pass->password); die();
		if ($old_pass->password != $_POST['old_password']) {
			$msg_data = array('alert_msg' => 'Incorrect old password', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
			redirect('admin/admin');
		}
		else {
			// redirect('admin/admin');
		}
		// var_dump($old_pass); die();	
		// var_dump($this->input->post()); die();
	}

	public function deleteAdmin($id)
	{
		if ($this->admin_model->deleteAdmin($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/admin');
		}
	}
/* Admin END */

/* Users */
	public function users() 
	{
		$this->dynamic_table = true;
		$data['users'] = $this->admin_model->getUsers();
		$this->wrapper('users', $data);
	}

	public function updateUser()
	{
		if ($this->admin_model->updateUser($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/users');
	}

	public function deleteUser($id) {
		if ($this->admin_model->deleteUser($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/users');
		}
	}
/* Users END */

/* Videos */
	public function videos() { # display videos page
		$this->dynamic_table = true;
		$data['videos'] = $this->admin_model->getVideos();
		$this->wrapper('videos', $data);
	}

	public function addVideo() {
		$new_filename = time(); #thumbnail new filename
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] = $new_filename;
		$this->load->library('upload', $config); # load upload library

		if (!$this->upload->do_upload('thumbnail')) # fail upload
		{
			# TODO change response/path?
			$error = array('error' => $this->upload->display_errors());
			die(var_dump($error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			// $_POST['thumbnail'] = $new_filename . $this->upload->data('file_ext');
			$_POST['thumbnail'] = base_url() . "uploads/" . $new_filename . $this->upload->data('file_ext');
			if ($_POST['is_featured'] == 1) {
				$_POST['date_featured'] = date("Y-m-d H:i:s");
			}
			$_POST['date_added'] = date("Y-m-d H:i:s");
			if ($this->admin_model->addVideo($this->input->post())) {
				$msg_data = array('alert_msg' => 'Video Added', 'alert_color' => 'green');
				$this->session->set_flashdata($msg_data);
			}
			else {
				$msg_data = array('alert_msg' => 'Failed to add video', 'alert_color' => 'red');
				$this->session->set_flashdata($msg_data);
			}
			redirect('admin/videos');
		}
	}

	public function updateVideo($id) {
		if ($_FILES['thumbnail']['name'] != "") {
			$new_filename = time();
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_filename;
			$this->load->library('upload', $config);
			$this->upload->do_upload('thumbnail');
			$_POST['thumbnail'] = base_url() . "uploads/" . $new_filename . $this->upload->data('file_ext');
		}
		

		$_POST['video_id'] = $id;
		if ($_POST['is_featured'] == 1) {
			$_POST['date_featured'] = date("Y-m-d H:i:s");
		}

		if ($this->admin_model->updateVideo($this->input->post())) {
			$msg_data = array('alert_msg' => 'Video updated', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to update video', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/videos');
	}

	public function deleteVideo($id) {
		if ($this->admin_model->deleteVideo($id)) {
			$msg_data = array('alert_msg' => 'Video deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/videos');
		}
	}

	public function video($id) {
		$data = $this->admin_model->getVideo($id);
		$this->wrapper('edit-video', $data);
	}
/* Videos END */

/* News/Blog */
	public function news() { # display news/blogs page
		$this->dynamic_table = true;
		$data['news'] = $this->admin_model->getNews();
		$this->wrapper('news', $data);
	}

	public function addNews() {
		$new_filename = time(); #thumbnail new filename
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] = $new_filename;
		$this->load->library('upload', $config); # load upload library

		if (!$this->upload->do_upload('thumbnail')) # fail upload
		{
			# TODO change response/path?
			$error = array('error' => $this->upload->display_errors());
			die(var_dump($error));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			$_POST['thumbnail'] = base_url() . "uploads/" . $new_filename . $this->upload->data('file_ext');
			if ($_POST['is_featured'] == 1) {
				$_POST['date_featured'] = date("Y-m-d H:i:s");
			}
			$_POST['date_added'] = date("Y-m-d H:i:s");

			if ($this->admin_model->addNews($this->input->post())) {
				$msg_data = array('alert_msg' => 'News Added', 'alert_color' => 'green');
				$this->session->set_flashdata($msg_data);
			}
			else {
				$msg_data = array('alert_msg' => 'Failed to add news', 'alert_color' => 'red');
				$this->session->set_flashdata($msg_data);
			}
			
			redirect('admin/news');
		}
	}

	public function updateNews($id) {
		if ($_FILES['thumbnail']['name'] != "") {
			$new_filename = time();
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_filename;
			$this->load->library('upload', $config);
			$this->upload->do_upload('thumbnail');
			$_POST['thumbnail'] = base_url() . "uploads/" . $new_filename . $this->upload->data('file_ext');
		}
		
		$_POST['news_id'] = $id;
		if ($_POST['is_featured'] == 1) {
			$_POST['date_featured'] = date("Y-m-d H:i:s");
		}

		if ($this->admin_model->updateNews($this->input->post())) {
			$msg_data = array('alert_msg' => 'News updated', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to update news', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/news');
	}

	public function deleteNews($id) {
		if ($this->admin_model->deleteNews($id)) {
			$msg_data = array('alert_msg' => 'News deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/news');
		}
	}

	public function news_blog($id) {
		$data = $this->admin_model->getNewsById($id);
		$this->wrapper('edit-news', $data);
	}
/* News/Blog END */

/* Products */
	public function products() {
		$this->dynamic_table = true;
		$this->multiple_select = true;
		$data['products'] = $this->admin_model->getProducts();
		$data['brands'] = $this->admin_model->getBrands();
		$data['categories'] = $this->admin_model->getCategories();
		$data['sub_categories'] = $this->admin_model->getSubCategories();
		$data['applications'] = $this->admin_model->getApplications();
		$data['sub_applications'] = $this->admin_model->getSubApplications();

 		$this->wrapper('products', $data);
	}

	public function addProduct() {
		if (isset($_POST['notif'])) {
			$notif = $_POST['notif'];
			unset($_POST['notif']);
		}
		else {
			$notif = false;
		}
		$new_filename = time();
		$config['upload_path'] = './uploads/product/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] = $new_filename;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('image')) {
			# TODO change response/path?
			$error = array('error' => $this->upload->display_errors());
			die(var_dump($error));
		}
		else {
			$data = array('upload_data' => $this->upload->data());

			$_POST['image'] = base_url() . "uploads/product/" . $new_filename . $this->upload->data('file_ext');
			if ($_POST['is_featured'] == 1) {
				$_POST['date_featured'] = date("Y-m-d H:i:s");
			}
			$_POST['date_added'] = date("Y-m-d H:i:s");
			
			$product_id = $this->admin_model->addProduct($this->input->post());
			if ($product_id) {
				if ($notif) {
					$this->sendNewProductNotification($product_id, $_POST['name']);
				}
				$msg_data = array('alert_msg' => 'Product Added', 'alert_color' => 'green');
				$this->session->set_flashdata($msg_data);
			}
			else {
				$msg_data = array('alert_msg' => 'Failed to add product', 'alert_color' => 'red');
				$this->session->set_flashdata($msg_data);
			}
			redirect('admin/products');
		}
	}

	public function product($id) {
		$this->multiple_select = true;
		
		$data['product'] = $this->admin_model->getProductById($id);
		$data['categories'] = $this->admin_model->getCategories();
		$data['brands'] = $this->admin_model->getBrands();
		$data['sub_categories'] = $this->admin_model->getSubCategories();
		$data['applications'] = $this->admin_model->getApplications();
		$data['sub_applications'] = $this->admin_model->getSubApplications();
		$this->wrapper('edit-product', $data);
	}

	public function updateProduct($id) {
		if ($_FILES['image']['name'] != "") {
			$new_filename = time();
			$config['upload_path'] = './uploads/product/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_filename;
			$this->load->library('upload', $config);
			$this->upload->do_upload('image');
			$_POST['image'] = base_url() . "uploads/product/" . $new_filename . $this->upload->data('file_ext');
		}

		$_POST['product_id'] = $id;
		if ($_POST['is_featured'] == 1) {
			$_POST['date_featured'] = date("Y-m-d H:i:s");
		}
		
		if ($this->admin_model->updateProduct($this->input->post())) {
			$msg_data = array('alert_msg' => 'Product updated', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to update product', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/products');
	}

	public function deleteProduct($id) {
		if ($this->admin_model->deleteProduct($id)) {
			$msg_data = array('alert_msg' => 'Product deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/products');
		}
	}
/* Products END */

/* Branches */
	public function branches()
	{
		$this->dynamic_table = true;
		$data['branches'] = $this->admin_model->getBranches();
		$data['countries'] = $this->admin_model->getCountries();
		$this->wrapper('branches', $data);
	}

	public function addBranch()
	{
		if ($this->input->post('is_main_branch')) {
			#reset all is_main_branch_column with country_id
			$this->admin_model->resetMainBranch($this->input->post('country_id'));
		}
		$this->admin_model->addBranch($this->input->post());
		redirect('admin/branches');
	}

	public function updateBranch($id)
	{
		$_POST['branch_id'] = $id;
		if ($this->input->post('is_main_branch')) {
			#reset all is_main_branch_column with country_id
			$this->admin_model->resetMainBranch($this->input->post('country_id'));
		}
		$this->admin_model->updateBranch($this->input->post());
		redirect('admin/branches');
	}

	public function addBranchSchedule($id)
	{
		$_POST['branch_id'] = $id;
		$this->admin_model->addBranchSchedule($this->input->post());
		redirect('admin/branch/'.$id);
	}

	public function deleteBranchSchedule($id)
	{
		if ($this->admin_model->deleteBranchSchedule($id)) {
			redirect('admin/branch/'.$_POST['branch_id']);
		}
	}

	public function branch($id)
	{
		$this->time_picker = true;
		$data['branch'] = $this->admin_model->getBranch($id);
		$data['countries'] = $this->admin_model->getCountries();
		$data['schedule'] = $this->admin_model->getSchedule($id);
		$this->wrapper('edit-branch', $data);
	}

	public function deleteBranch($id)
	{
		if ($this->admin_model->deleteBranch($id)) {
			redirect('admin/branches');
		}
	}
/* Branches END */

/* Inquiries */
	public function inquiries()
	{
		$this->dynamic_table = true;
		$data['inquiries'] = $this->admin_model->getInquiries();
		$this->wrapper('inquiries', $data);
	}
/* Inquiries END */

/* Countries */
	public function countries()
	{
		$this->dynamic_table = true;
		$data['countries'] = $this->admin_model->getCountries();
		$this->wrapper('countries', $data);
	}

	public function country($id)
	{
		$data['country'] = $this->admin_model->getCountry($id);
		$this->wrapper('edit-country', $data);
	}

	public function addCountry()
	{
		if ($this->admin_model->addCountry($this->input->post())) {
			$msg_data = array('alert_msg' => 'Country Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add country', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/countries');
	}

	public function updateCountry()
	{
		if ($this->admin_model->updateCountry($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}

		redirect('admin/countries');
	}

	public function deleteCountry($id)
	{
		if ($this->admin_model->deleteCountry($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/countries');
		}
	}
/* Countries END */

/* Recipes */
	public function recipes()
	{
		$this->dynamic_table = true;
		$data['recipes'] = $this->admin_model->getRecipes();
		$data['categories'] = $this->admin_model->getCategories();
		$data['products'] = $this->admin_model->getProducts();
		$this->wrapper('recipes', $data);
	}

	public function recipe($id)
	{
		$data['recipes'] = $this->admin_model->getRecipe($id);
		
		$data['other_ingredients'] = $this->admin_model->getOtherIngredientsByRecipe($data['recipes']->recipe_id);
		$data['ingredients'] = $this->admin_model->getIngredientsByRecipe($data['recipes']->recipe_id);
		$data['categories'] = $this->admin_model->getCategories();
		$data['products'] = $this->admin_model->getProducts();
		$this->wrapper('edit-recipe', $data);
	}

	public function updateRecipe($id)
	{
		if ($_FILES['image']['name'] != "") {
			$new_filename = time();
			$config['upload_path'] = './uploads/recipe/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name'] = $new_filename;
			$this->load->library('upload', $config);
			$this->upload->do_upload('image');
			$_POST['image'] = base_url() . "uploads/recipe/" . $new_filename . $this->upload->data('file_ext');
		}

		$_POST['recipe_id'] = $id;
		if ($_POST['is_featured'] == 1) {
			$_POST['date_featured'] = date("Y-m-d H:i:s");
		}
		$_POST['date_added'] = date("Y-m-d H:i:s");
		
		if ($this->admin_model->updateRecipe($this->input->post())) {
			$msg_data = array('alert_msg' => 'Recipe updated', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to update recipe', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/recipes');
	}

	public function deleteRecipe($id) 
	{
		if ($this->admin_model->deleteRecipe($id)) {
			$msg_data = array('alert_msg' => 'Recipe deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/recipes');
		}
	}

	public function addRecipe()
	{
		$new_filename = time();
		$config['upload_path'] = './uploads/recipe/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png';
		$config['file_name'] = $new_filename;
		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('image')) {
			# TODO change response/path?
			$error = array('error' => $this->upload->display_errors());
			die(var_dump($error));
		}
		else {
			$data = array('upload_data' => $this->upload->data());
			$_POST['image'] = base_url() . "uploads/recipe/" . $new_filename . $this->upload->data('file_ext');

			if ($_POST['is_featured'] == 1) {
				$_POST['date_featured'] = date("Y-m-d H:i:s");
			}
			$_POST['date_added'] = date("Y-m-d H:i:s");
			
			if ($this->admin_model->addRecipe($this->input->post())) {
				$msg_data = array('alert_msg' => 'Recipe Added', 'alert_color' => 'green');
				$this->session->set_flashdata($msg_data);
			}
			else {
				$msg_data = array('alert_msg' => 'Failed to add recipe', 'alert_color' => 'red');
				$this->session->set_flashdata($msg_data);
			}
			redirect('admin/recipes');
		}
	}
/* Recipes END */

/* Configuration (Brands, Categories, Applications) */
	/* Brands */
	public function brands()
	{
		$this->dynamic_table = true;
		$data['brands'] = $this->admin_model->getBrands();
		$this->wrapper('brands', $data);
	}

	public function brand($id)
	{
		$data['brand'] = $this->admin_model->getBrand($id);
		$this->wrapper('edit-brand', $data);
	}

	public function addBrand()
	{
		if ($this->admin_model->addBrand($this->input->post())) {
			$msg_data = array('alert_msg' => 'Brand Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add brand', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/brands');
	}

	public function updateBrand()
	{
		if ($this->admin_model->updateBrand($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/brands');
	}

	public function deleteBrand($id)
	{
		if ($this->admin_model->deleteBrand($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/brands');
		}
	}
	/* Brands */

	/* Categories */
	public function categories()
	{
		$this->dynamic_table = true;
		$data['categories'] = $this->admin_model->getCategories();
		$data['sub_categories'] = $this->admin_model->getSubCategories();
		$this->wrapper('categories', $data);
	}

	public function category($id)
	{
		$data['categories'] = $this->admin_model->getCategories();
		$data['sub_category'] = $this->admin_model->getSubCategory($id);
		$this->wrapper('edit-sub-category', $data);
	}

	public function addCategory()
	{
		if ($this->admin_model->addCategory($this->input->post())) {
			$msg_data = array('alert_msg' => 'Category Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add category', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/categories');
	}

	public function addSubCategory()
	{
		if ($this->admin_model->addSubCategory($this->input->post())) {
			$msg_data = array('alert_msg' => 'Sub-category Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add sub-category', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/categories');
	}

	public function updateCategory()
	{
		if ($this->admin_model->updateCategory($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/categories');
	}

	public function updateSubCategory($id)
	{
		$_POST['sub_cat_id'] = $id;
		if ($this->admin_model->updateSubCategory($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/categories');
	}

	public function deleteCategory($id)
	{
		if ($this->admin_model->deleteCategory($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/categories');
		}
	}

	public function deleteSubCategory($id)
	{
		if ($this->admin_model->deleteSubCategory($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/categories');
		}
	}
	/* Categories */

	/* Applications */
	public function applications()
	{
		$this->dynamic_table = true;
		$data['applications'] = $this->admin_model->getApplications();
		$data['sub_applications'] = $this->admin_model->getSubApplications();
		$this->wrapper('applications', $data);
	}

	public function application($id)
	{
		$data['applications'] = $this->admin_model->getApplications();
		$data['sub_application'] = $this->admin_model->getSubApplication($id);
		$this->wrapper('edit-sub-application', $data);
	}

	public function addApplication()
	{
		if ($this->admin_model->addApplication($this->input->post())) {
			$msg_data = array('alert_msg' => 'Application Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add application', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/applications');
	}

	public function addSubApplication()
	{
		if ($this->admin_model->addSubApplication($this->input->post())) {
			$msg_data = array('alert_msg' => 'Sub-application Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add sub-application', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/applications');
	}

	public function updateApplication()
	{
		if ($this->admin_model->updateApplication($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/applications');
	}

	public function updateSubApplication($id)
	{
		$_POST['sub_app_id'] = $id;
		if ($this->admin_model->updateSubApplication($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/applications');
	}

	public function deleteApplication($id)
	{
		if ($this->admin_model->deleteApplication($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/applications');
		}
	}

	public function deleteSubApplication($id)
	{
		if ($this->admin_model->deleteSubApplication($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/applications');
		}
	}
	/* Applications */

	/* Segments */
	public function segments()
	{
		$this->dynamic_table = true;
		$data['segments'] = $this->segment_model->getSegments();
		$this->wrapper('segments', $data);
	}

	public function addSegment()
	{
		if ($this->segment_model->addSegment($this->input->post())) {
			$msg_data = array('alert_msg' => 'Segment Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add segment', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/segments');
	}

	public function updateSegment()
	{
		if ($this->segment_model->updateSegment($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/segments');
	}

	public function deleteSegment($id)
	{
		if ($this->segment_model->deleteSegment($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/segments');
		}
	}
	/* Segments */
/* Configuration END */

/* Dealers */
	public function dealers()
	{
		$this->dynamic_table = true;
		$data['dealers'] = $this->dealer_model->getDealers();
		$this->wrapper('dealers', $data);
	}

	public function addDealer()
	{
		if ($this->dealer_model->addDealer($this->input->post())) {
			$msg_data = array('alert_msg' => 'Dealer Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add dealer', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/dealers');
	}

	public function updateDealer()
	{
		if ($this->dealer_model->updateDealer($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/dealers');
	}

	public function deleteDealer($id)
	{
		if ($this->dealer_model->deleteDealer($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/dealers');
		}
	}
/* Dealers END */

/* Sales Team */
	public function sales()
	{
		$this->dynamic_table = true;
		$data['sales_team'] = $this->sales_model->getSalesTeam();
		$this->wrapper('sales-team', $data);
	}

	public function addSales()
	{
		if ($this->sales_model->addSales($this->input->post())) {
			$msg_data = array('alert_msg' => 'Sales Added', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Failed to add sales', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/sales');
	}

	public function updateSales()
	{
		if ($this->sales_model->updateSales($this->input->post())) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
			$this->session->set_flashdata($msg_data);
		}
		redirect('admin/sales');
	}

	public function deleteSales($id)
	{
		if ($this->sales_model->deleteSales($id)) {
			$msg_data = array('alert_msg' => 'Successfully deleted', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/sales');
		}
	}
/* Sales Team END */

/* Notifications */
	public function announcements()
	{
		$this->wrapper('announcements');
	}
/* Notifications END*/


	public function testNotification()
	{
		$this->wrapper('test-notification');
	}

	public function sendNewProductNotification($product_id, $product_name)
	{
		require_once(APPPATH.'libraries/firebase.php');

		$firebase = new Firebase();

		$push_type = "topic";
		$title = "New Product";
		$message = $product_name;
		$category = "product";

		$res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = $category;
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
        $res['data']['product_id'] = $product_id;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');

        if ($push_type == 'topic') {
            $notification['title'] = $title;
            $notification['body'] = $message;
            $response = $firebase->sendToTopic('public', $res, $notification);
            $msg_data = array('alert_msg' => 'Product added and notification sent', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/products');
        }
	}	

	public function sendNotification()
	{
		require_once(APPPATH.'libraries/firebase.php');

		$firebase = new Firebase();

		$push_type = "topic";
		$title = $_POST['title'];
		$message = $_POST['message'];
		$category = $_POST['category'];

		$res = array();
        $res['data']['status'] = true;
        $res['data']['category'] = $category;
        $res['data']['title'] = $title;
        $res['data']['message'] = $message;
        $res['data']['timestamp'] = date('Y-m-d G:i:s');

        if ($push_type == 'topic') {
            $notification['title'] = $title;
            $notification['body'] = $message;
            $response = $firebase->sendToTopic('public', $res, $notification);
            $msg_data = array('alert_msg' => 'Notification successful', 'alert_color' => 'green');
			$this->session->set_flashdata($msg_data);
			redirect('admin/announcements');
        }
	}
}